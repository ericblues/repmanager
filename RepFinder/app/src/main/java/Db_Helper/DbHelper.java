package Db_Helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Shared_Preferences.shHelper;

/**
 * Created by eric on 16/10/17.
 */

public class DbHelper extends AppCompatActivity {

    private SQLiteDatabase bancoDados;

    private ListView lista;

    private shHelper sh = null;

    public DbHelper(Context context){
        try{
            bancoDados = context.openOrCreateDatabase("REPSYS.db", Context.MODE_PRIVATE, null);
            sh = new shHelper(context);
        }catch (Exception e){
            Log.i("ERRO CRIACAO BANCO: ", e.toString());
        }
    }


    public void criaTabela(String nomeTabela, String camposTipos){

        String campos = camposTipos;


        try {
            bancoDados.execSQL("CREATE TABLE IF NOT EXISTS "+nomeTabela+"("+campos+")");
            Log.i("INFO: ", "TABELA CRIADA COM SUCESSO");
            System.out.println("INFO: TABELA CRIADA COM SUCESSO");
            //bancoDados.execSQL("CREATE TABLE IF NOT EXISTS CARALHO(id_tarefa INTEGER PRIMARY KEY AUTOINCREMENT);");
        }catch (Exception e){
            Log.i("ERRO TABELA: ", e.toString());
        }
    }


    //Inserção de dados nas tabelas do banco de dados
    //Recebe a instrução sql como parâmetro
    public void insereDados(String insert){
        try {
            bancoDados.execSQL(insert);
            bancoDados.close();
            Log.i("INFO: ", "INSERÇÃO REALIZADA COM SUCESSO");
            System.out.println("INFO: INSERÇÃO REALIZADA COM SUCESSO");
        }catch (Exception e){
            Log.i("ERRO INSERIR: ", e.toString());
        }

    }


    //Método genérico de atualização de dados
    //Recebe como parâmetro a instrução sql completa com os campos e dados a serem atualizados
    //Consulta dos dados, mostrando os dados no Log em tempo de execução
    public void atualizarDados(String strSQL){
        try {
            bancoDados.execSQL(strSQL);
            bancoDados.close();
            Log.i("INFO: ", "ATUALIZAÇÃO REALIZADA COM SUCESSO");
            System.out.println("INFO: ATUALIZAÇÃO REALIZADA COM SUCESSO");
        }catch (Exception e){
            Log.i("ERRO ATUALIZAR: ", e.toString());
        }

    }


    public void excluirDados(String strSQL){
        try{
            bancoDados.execSQL(strSQL);
            bancoDados.close();
            Log.i("INFO: ", "EXCLUSÃO REALIZADA COM SUCESSO");
            System.out.println("EXCLUSÃO: ATUALIZAÇÃO REALIZADA COM SUCESSO");
        }catch (Exception e){
            Log.i("ERRO EXCLUIR: ", e.toString());
        }

    }

    public List<HashMap<String, String>> consultaTarefas(){

        List<HashMap<String, String>> listaTarefas = new ArrayList<>();

        Cursor cursor;

        try {
            cursor = bancoDados.rawQuery("SELECT id_tarefa, id_tarefa_servidor, titulo_tarefa, desc_tarefa, tipo_tarefa FROM TAREFAS", null);

            int idTarefa = cursor.getColumnIndex("id_tarefa");
            int idServidor = cursor.getColumnIndex("id_tarefa_servidor");
            int idTitulo = cursor.getColumnIndex("titulo_tarefa");
            int idDesc = cursor.getColumnIndex("desc_tarefa");
            int idTipo = cursor.getColumnIndex("tipo_tarefa");

            cursor.moveToFirst();

            do{
                //cursor.moveToNext();

                HashMap<String, String> tarefa = new HashMap<>();

                tarefa.put("id_tarefa", cursor.getString(idTarefa));
                tarefa.put("id_tarefa_servidor", cursor.getString(idServidor));
                tarefa.put("titulo_tarefa", cursor.getString(idTitulo));
                tarefa.put("titulo_tarefa_info", "Tarefa: " + cursor.getString(idTitulo));
                tarefa.put("desc_tarefa", cursor.getString(idDesc));
                tarefa.put("desc_tarefa_info", "Desc: " + cursor.getString(idDesc));
                tarefa.put("tipo_tarefa", cursor.getString(idTipo));
                tarefa.put("tipo_tarefa_info", "Tipo: " + cursor.getString(idTipo));

                listaTarefas.add(tarefa);


                Log.i("ID: ", cursor.getString(idTarefa));
                Log.i("TÍTULO: ", cursor.getString(idTitulo));
                Log.i("DESCRIÇÃO: ", cursor.getString(idDesc));
                Log.i("TIPO: ", cursor.getString(idTipo));


            }while (cursor.moveToNext());

            if (cursor != null && !cursor.isClosed()){
                cursor.close();
            }

        }catch (Exception e){
            Log.i("ERRO CONSULTA: ", e.toString());
        }
        return listaTarefas;
    }


    public List<HashMap<String, String>> consultaAtbTarefas(){

        List<HashMap<String, String>> listaTarefas = new ArrayList<>();

        Cursor cursor;

        try {
            cursor = bancoDados.rawQuery("SELECT * FROM ATB_TAREFAS WHERE id_usuario = " + sh.resgatarCampo("id_usuario"), null);

            int idTarefa = cursor.getColumnIndex("id_atb_tarefa_local");
            int idServidor = cursor.getColumnIndex("id_atb_tarefa_servidor");
            int idTitulo = cursor.getColumnIndex("titulo_tarefa");
            int idDesc = cursor.getColumnIndex("desc_tarefa");
            int idTipo = cursor.getColumnIndex("tipo_tarefa");

            cursor.moveToFirst();

            do{
                //cursor.moveToNext();

                HashMap<String, String> tarefa = new HashMap<>();

                tarefa.put("id_atb_tarefa_local", cursor.getString(idTarefa));
                tarefa.put("id_atb_tarefa_servidor", cursor.getString(idServidor));
                tarefa.put("titulo_tarefa", cursor.getString(idTitulo));
                tarefa.put("titulo_tarefa_info", "Tarefa: " + cursor.getString(idTitulo));
                tarefa.put("desc_tarefa", cursor.getString(idDesc));
                tarefa.put("desc_tarefa_info", "Desc: " + cursor.getString(idDesc));
                tarefa.put("tipo_tarefa", cursor.getString(idTipo));
                tarefa.put("tipo_tarefa_info", "Tipo: " + cursor.getString(idTipo));

                listaTarefas.add(tarefa);


                Log.i("ID: ", cursor.getString(idTarefa));
                Log.i("TÍTULO: ", cursor.getString(idTitulo));
                Log.i("DESCRIÇÃO: ", cursor.getString(idDesc));
                Log.i("TIPO: ", cursor.getString(idTipo));


            }while (cursor.moveToNext());

            if (cursor != null && !cursor.isClosed()){
                cursor.close();
            }

        }catch (Exception e){
            Log.i("ERRO CONSULTA: ", e.toString());
        }
        return listaTarefas;
    }


    public List<HashMap<String, String>> consultarMinhasTarefas(String idTarefa, String id_usuario){


        List<HashMap<String, String>> listaTarefas = new ArrayList<>();

        Cursor cursor;

        try {
            Log.i("ID PARA CONSULTAR: ", idTarefa);
            cursor = bancoDados.rawQuery("SELECT * FROM ATB_TAREFAS WHERE id_tarefa = " + idTarefa + " AND id_usuario = " + id_usuario, null);

            int idATbTarefaLocal = cursor.getColumnIndex("id_atb_tarefa_local");
            int idAtbTarefaServidor = cursor.getColumnIndex("id_atb_tarefa_servidor");
            int diasSemana = cursor.getColumnIndex("dias_semana");

            cursor.moveToFirst();

            do{
                //cursor.moveToNext();

                HashMap<String, String> tarefa = new HashMap<>();

                tarefa.put("id_atb_tarefa_local", cursor.getString(idATbTarefaLocal));
                tarefa.put("id_atb_tarefa_servidor", cursor.getString(idAtbTarefaServidor));
                tarefa.put("dias_semana", cursor.getString(diasSemana));

                listaTarefas.add(tarefa);

            }while (cursor.moveToNext());

            if (cursor != null && !cursor.isClosed()){
                cursor.close();
            }

        }catch (Exception e){
            Log.i("ERRO CONSULTA: ", e.toString());
        }
        return listaTarefas;
    }

    public List<HashMap<String, String>> consultaDespesas(){

        List<HashMap<String, String>> listaDespesas = new ArrayList<>();

        Cursor cursor;

        try {
            cursor = bancoDados.rawQuery("SELECT id_despesa, titulo_despesa, desc_despesa, valor_despesa, vencimento_despesa, frequencia, repeticao FROM DESPESAS WHERE id_administrador_republica = " + sh.resgatarCampo("id_administrador_republica"), null);

            int idDespesa = cursor.getColumnIndex("id_despesa");
            int idTitulo = cursor.getColumnIndex("titulo_despesa");
            int idDesc = cursor.getColumnIndex("desc_despesa");
            int idValor = cursor.getColumnIndex("valor_despesa");
            int idVencimento = cursor.getColumnIndex("vencimento_despesa");
            int idFrequencia = cursor.getColumnIndex("frequencia");
            int idRepeticao = cursor.getColumnIndex("repeticao");

            cursor.moveToFirst();

            do{
                //cursor.moveToNext();

                HashMap<String, String> despesa = new HashMap<>();

                despesa.put("id_despesa", cursor.getString(idDespesa));
                despesa.put("titulo_despesa", cursor.getString(idTitulo));
                despesa.put("desc_despesa", cursor.getString(idDesc));
                despesa.put("valor_despesa", cursor.getString(idValor));
                despesa.put("vencimento_despesa", cursor.getString(idVencimento));
                despesa.put("frequencia", cursor.getString(idFrequencia));
                despesa.put("repeticao", cursor.getString(idRepeticao));

                listaDespesas.add(despesa);

                Log.i("ID ..........: ", cursor.getString(idDespesa));
                Log.i("TÍTULO ......: ", cursor.getString(idTitulo));
                Log.i("DESCRIÇÃO ...: ", cursor.getString(idDesc));
                Log.i("VALOR .......: ", cursor.getString(idValor));
                Log.i("VENCIMENTO ..: ", cursor.getString(idVencimento));
                Log.i("FREQUENCIA ..: ", cursor.getString(idFrequencia));
                Log.i("REPETICAO ...: ", cursor.getString(idRepeticao));


            }while (cursor.moveToNext());

            if (cursor != null && !cursor.isClosed()){
                cursor.close();
                bancoDados.close();
            }

        }catch (Exception e){
            Log.i("Mensagem de erro: ", e.toString());
        }
        return listaDespesas;
    }

}
