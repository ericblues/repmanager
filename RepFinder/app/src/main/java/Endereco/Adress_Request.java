package Endereco;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by eric on 21/08/17.
 */

public class Adress_Request{


    private JsonObjectRequest requisicao = null;
    private HashMap<String, String> dados_endereco = new HashMap<>();
    private boolean retorno;

    public void consultaCEP(String cep, Context context){

        String URL = "https://viacep.com.br/ws/"+cep+"/json/";

        requisicao = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.has("erro")){
                        Log.i("Erro: ", response.toString());
                    }else{
                        separaLista(response);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Erro: ", error.toString());
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(requisicao);
    }

    private void separaLista(JSONObject dados) throws JSONException{

        dados_endereco.put("cep", dados.getString("cep"));
        dados_endereco.put("rua", dados.getString("logradouro"));
        dados_endereco.put("complemento", dados.getString("complemento"));
        dados_endereco.put("bairro", dados.getString("bairro"));
        dados_endereco.put("cidade", dados.getString("localidade"));
        dados_endereco.put("estado", dados.getString("uf"));

    }

    public HashMap<String, String> getDados_endereco() {
        return dados_endereco;
    }

    public void setDados_endereco(HashMap<String, String> dados_endereco) {
        this.dados_endereco = dados_endereco;
    }
}
