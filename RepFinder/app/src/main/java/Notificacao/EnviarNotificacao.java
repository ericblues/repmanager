package Notificacao;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import Shared_Preferences.shHelper;
import repfinder.repsys.com.repfinder.TabTarefas;
import repfinder.repsys.com.repfinder.cadastroTarefa;

/**
 * Created by eric on 03/10/17.
 */

public class EnviarNotificacao {
    private Context context;
    private String token = "AAAAX6RwHbQ:APA91bGkPmo8ThmNG7bHW6k5A1iZ3J0nvHyOzqtJFiyYsp3Nl2Vh7J4KGjAKZqgwIJ9VOmE1ZB5myfkAm_z8I4_YnvgLXqqojuNr77PTsc1ZEcyXnGULf249YcNwlIcA51dTy8-0BP7U";
    private String tokenGabs = "cT56mEbqVas:APA91bFC4YklW3Vebeyi_581IbRJ9k3uCykMoGO0E2hDAmRl6lWQlo0PyokNq5wscRSSjMaM1fJ7OvmiqERvBaNWxhMxNY112Wo7xhKoegoCIC10ZDnor4u_0xcY4rL8v1NYngEa2B4s";
    public EnviarNotificacao(Context context){
        this.context = context;
        try {
            Request();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Objeto de requisição padrão
    private JsonObjectRequest requisicao = null;

    //CODIGO DE RESPOSTA DO SERVIDOR
    private int status_code;
    private shHelper sh;

    private void Request() throws JSONException{

        JSONObject valores = new JSONObject();

        valores.put("mensagem", "Aeeeeeoooow");

        JSONObject json = new JSONObject();

        json.put("notification", valores);
        json.put("to", tokenGabs);





        requisicao = new JsonObjectRequest(Request.Method.POST, "https://fcm.googleapis.com/fcm/send", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {


                Log.i("RETORNO: ", response.toString());
                Log.i("CÓDIGO: ", String.valueOf(status_code));

                try {
                    if (response.getString("success").equals("1"))
                        Log.i("RESPOSTA: ", "ENVIADO COM SUCESSO");
                } catch (JSONException e) {
                    Log.i("ERRO: ", e.toString());
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Erro: ", error.toString());
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "key=" + token);
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(requisicao);
    }
}
