package Servidor;

import android.content.Context;

import Shared_Preferences.shHelper;

/**
 * Created by eric on 22/08/17.
 */

public class server_URL {

    private Context contexto;
    private shHelper sh;
    private String URL;

    public server_URL(Context context){
        this.contexto = context;
    }

    public String getURL(String operacao){

        sh = new shHelper(contexto);
        URL = "http://"+sh.resgatarIP()+"/api/";

        switch (operacao){
            case "login":
                URL += "auth/login";
                break;
            case "cadastro":
                URL += "auth/usuario";
                break;
            case "cadastroRepublica":
                URL += "republica";
                break;
            case "tarefa":
                URL += "republica/tarefa";
                break;
            case "despesa":
                URL += "republica/despesa";
                break;
            case "republica":
                URL += "republica/buscar";
                break;
            case "morador":
                URL += "republica/morador";
                break;
            case "atbTarefa":
                URL += "republica/tarefa/atribuicao";
                break;
            case "disponibilidade":
                URL += "republica/tarefa/atribuicao";
                break;
            case "atualiza_atbTarefa":
                URL += "republica/tarefa/atualizaAtribuicao";
            case "MAC":
                URL += "usuario/mac";
                break;
            case "carregarDados":
                URL += "usuario/getAllData";
            default:
                break;
        }

        return URL;
    }

}
