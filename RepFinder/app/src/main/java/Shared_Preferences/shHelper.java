package Shared_Preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Switch;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by eric on 29/08/17.
 */

public class shHelper {

    private Context contexto;

    public shHelper(Context contexto){
        this.contexto = contexto;
    }

    public void salvarIP(String ip){

        SharedPreferences.Editor dadosServidor = contexto.getSharedPreferences("dadosServidor", contexto.MODE_PRIVATE).edit();

        dadosServidor.putString("ip", ip);

        dadosServidor.commit();
    }


    public String resgatarIP(){

        SharedPreferences dadosServidor  = contexto.getSharedPreferences("dadosServidor", contexto.MODE_PRIVATE);
        return dadosServidor.getString("ip", null);

    }

    public void salvarToken(String token){
        SharedPreferences.Editor dadosServidor = contexto.getSharedPreferences("dadosServidor", contexto.MODE_PRIVATE).edit();

        dadosServidor.putString("token", token);

        dadosServidor.commit();
    }

    public String resgatarToken(){
        SharedPreferences dadosServidor  = contexto.getSharedPreferences("dadosServidor", contexto.MODE_PRIVATE);
        return dadosServidor.getString("token", null);
    }

    public void limparToken(){
        SharedPreferences.Editor dadosServidor = contexto.getSharedPreferences("dadosServidor", contexto.MODE_PRIVATE).edit();

        dadosServidor.putString("token", null);

        dadosServidor.commit();
    }

    public void salvarTarefa(String id_tarefa, String titulo_tarefa, String desc_tarefa, String tipo){
        SharedPreferences.Editor dadosTarefa = contexto.getSharedPreferences("dadosTarefa", contexto.MODE_PRIVATE).edit();

        dadosTarefa.putString("id_tarefa", id_tarefa);
        dadosTarefa.putString("titulo_tarefa", titulo_tarefa);
        dadosTarefa.putString("desc_tarefa", desc_tarefa);
        dadosTarefa.putString("tipo_tarefa", titulo_tarefa);

        dadosTarefa.commit();
    }

    public String resgatarTarefa(int opcao){

        String retorno = "";

        SharedPreferences dadosTarefa = contexto.getSharedPreferences("dadosTarefa", contexto.MODE_PRIVATE);

        switch (opcao){
            case 1:
                retorno = dadosTarefa.getString("id_tarefa", null);
                break;
            case 2:
                retorno = dadosTarefa.getString("titulo_tarefa", null);
                break;
            case 3:
                retorno = dadosTarefa.getString("desc_tarefa", null);
                break;
            case 4:
                retorno = dadosTarefa.getString("tipo_tarefa", null);
                break;
        }

        return retorno;
    }


    public void salvarDespesa(String id_despesa, String titulo_despesa, String valor_despesa){
        SharedPreferences.Editor dadosDespesa = contexto.getSharedPreferences("dadosDespesa", contexto.MODE_PRIVATE).edit();

        dadosDespesa.putString("id_despesa", id_despesa);
        dadosDespesa.putString("titulo_despesa", titulo_despesa);
        dadosDespesa.putString("valor_despesa", valor_despesa);

        dadosDespesa.commit();
    }

    public String resgatarDespesa(int opcao){

        String retorno = "";

        SharedPreferences dadosDespesa = contexto.getSharedPreferences("dadosDespesa", contexto.MODE_PRIVATE);

        switch (opcao){
            case 1:
                retorno = dadosDespesa.getString("id_despesa", null);
                break;
            case 2:
                retorno = dadosDespesa.getString("titulo_despesa", null);
                break;
            case 3:
                retorno = dadosDespesa.getString("valor_despesa", null);
                break;
        }

        return retorno;
    }



    public void salvarDiasSemana(String diasSemana){
        SharedPreferences.Editor dadosSemana = contexto.getSharedPreferences("dadosSemana", contexto.MODE_PRIVATE).edit();

        dadosSemana.putString("diasSemana", diasSemana);

        dadosSemana.commit();
    }


    public String resgatarDiasSemana(){
        SharedPreferences dadosSemana = contexto.getSharedPreferences("dadosSemana", contexto.MODE_PRIVATE);

        return dadosSemana.getString("diasSemana", null);
    }


    public void alocarDias(HashMap<String, String> diasSemana){
        SharedPreferences.Editor dadosGuardados = contexto.getSharedPreferences("diasAlocados", contexto.MODE_PRIVATE).edit();

        dadosGuardados.putString("dom", diasSemana.get("dom"));
        dadosGuardados.putString("seg", diasSemana.get("seg"));
        dadosGuardados.putString("ter", diasSemana.get("ter"));
        dadosGuardados.putString("qua", diasSemana.get("qua"));
        dadosGuardados.putString("qui", diasSemana.get("qui"));
        dadosGuardados.putString("sex", diasSemana.get("sex"));
        dadosGuardados.putString("sab", diasSemana.get("sab"));
        dadosGuardados.commit();

    }


    public String verificaDia(int numeroDia){


        Log.i("Dia Consultado: ", " " + numeroDia);

        SharedPreferences dadosRecuperados  = contexto.getSharedPreferences("diasAlocados", contexto.MODE_PRIVATE);
        switch (numeroDia){
            case 1:
                return dadosRecuperados.getString("dom", null);
            case 2:
                return dadosRecuperados.getString("seg", null);
            case 3:
                return dadosRecuperados.getString("ter", null);
            case 4:
                return dadosRecuperados.getString("qua", null);
            case 5:
                return dadosRecuperados.getString("qui", null);
            case 6:
                return dadosRecuperados.getString("sex", null);
            case 7:
                return dadosRecuperados.getString("sab", null);
            default:
                return null;
        }
    }


    private void usuario(JSONObject informacoes) throws JSONException {
        SharedPreferences.Editor dadosGuardados = contexto.getSharedPreferences("dadosUsuario", contexto.MODE_PRIVATE).edit();



        dadosGuardados.putString("id_usuario", informacoes.getString("id_usuario"));
        dadosGuardados.putString("nome_usuario", informacoes.getString("nome_usuario"));
        dadosGuardados.putString("sobrenome_usuario", informacoes.getString("sobrenome_usuario"));
        dadosGuardados.putString("tipo_usuario", informacoes.getString("tipo_usuario"));
        dadosGuardados.putString("email_usuario", informacoes.getString("email_usuario"));
        dadosGuardados.putString("primeiro_acesso", informacoes.getString("primeiro_acesso"));

        //dadosGuardados.putBoolean("logado", true);

        dadosGuardados.commit();

    }

    private void republica(JSONObject informacoes) throws JSONException{

        SharedPreferences.Editor dadosGuardados = contexto.getSharedPreferences("dadosUsuario", contexto.MODE_PRIVATE).edit();

        dadosGuardados.putString("id_republica", informacoes.getString("id_republica"));
        dadosGuardados.putString("nome_republica", informacoes.getString("nome_republica"));
        dadosGuardados.putString("tipo_republica", informacoes.getString("tipo_republica"));
        dadosGuardados.putString("valor_aluguel_republica", informacoes.getString("valor_aluguel_republica"));
        dadosGuardados.putString("anuncio_republica", informacoes.getString("anuncio_republica"));
        dadosGuardados.putString("qtde_vagas_republica", informacoes.getString("qtde_vagas_republica"));

        dadosGuardados.commit();
    }

    private void enderecoRepublica(JSONObject informacoes) throws JSONException{

        SharedPreferences.Editor dadosGuardados = contexto.getSharedPreferences("dadosUsuario", contexto.MODE_PRIVATE).edit();

        dadosGuardados.putString("id_endereco_adm", informacoes.getString("id_endereco"));
        dadosGuardados.putString("cep_endereco_adm", informacoes.getString("cep_endereco"));
        dadosGuardados.putString("rua_endereco_adm", informacoes.getString("rua_endereco"));
        dadosGuardados.putString("estado_endereco_adm", informacoes.getString("estado_endereco"));
        dadosGuardados.putString("numero_endereco_adm", informacoes.getString("numero_endereco"));
        dadosGuardados.putString("cidade_endereco_adm", informacoes.getString("cidade_endereco"));
        dadosGuardados.putString("bairro_endereco_adm", informacoes.getString("bairro_endereco"));

        dadosGuardados.commit();
    }

    public String resgatarCampo(String campo){
        //Método para resgatar campos únicos do shared Preferences, como exemplo, o id do usuário para um cadastro de república
        //O retorno será o valor do campos desejado como string, ou um valor nulo
        //O tratamento do retorno será feito na classe que está chamando a função
        SharedPreferences dadosRecuperados  = contexto.getSharedPreferences("dadosUsuario", contexto.MODE_PRIVATE);
        return dadosRecuperados.getString(campo, null);
    }

    public boolean limparSH(){

        SharedPreferences.Editor dadosGuardados = contexto.getSharedPreferences("dadosUsuario", contexto.MODE_PRIVATE).edit();

        dadosGuardados.clear();
        dadosGuardados.putBoolean("logado", false);

        dadosGuardados.commit();


        return true;
    }

    public void salvarDadosUsuario(JSONObject dadosUsuario, JSONObject dadosRepublica) throws JSONException {
        //O seguinte está acontecendo:
        //Na classe de login, estamos recebendo um objeto JSON contendo outros objetos dentro dele
        //Os objetos correspondem aos parâmetros recebidos nesse método
        //Para isso, na classe de login ocorre a separação do Objeto inteiro JSON, em partes
        //Cada parte do do inteiro é envidao para esse método, onde serão separados os dados do objeto para
        //inserção no sharedPreferences, denominado >> dadosGuardados


        if (dadosRepublica != null){
            usuario(dadosUsuario);
            republica(dadosRepublica);
        }else{
            usuario(dadosUsuario);
        }

    }

}
