package Tab_Despesas;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.List;

import repfinder.repsys.com.repfinder.R;

/**
 * Created by eric on 02/10/17.
 */

public class DespesaAdapter extends BaseAdapter {

    private List<HashMap<String, String>> despesas;
    private final Activity act;

    DespesaAdapter(List<HashMap<String, String>> despesas, Activity act){
        this.act = act;
        this.despesas = despesas;
    }

    @Override
    public int getCount() {

        return 0;
    }

    @Override
    public Object getItem(int position) {
        return despesas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = act.getLayoutInflater().inflate(R.layout.modelo_consulta_despesa, parent, false);

        TextView titulo_despesa = (TextView) view.findViewById(R.id.txtCAMPODESPESA1);
        TextView desc_despesa = (TextView) view.findViewById(R.id.txtCAMPODESPESA2);
        TextView valor_despesa = (TextView) view.findViewById(R.id.txtCAMPODESPESA3);

        titulo_despesa.setText(despesas.get(position).get("titulo_despesa"));
        desc_despesa.setText(despesas.get(position).get("desc_despesa"));
        valor_despesa.setText(despesas.get(position).get("valor_despesa"));

        return view;
    }
}
