package Tab_Despesas;


import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import Servidor.server_URL;
import Shared_Preferences.shHelper;
import repfinder.repsys.com.repfinder.R;
import repfinder.repsys.com.repfinder.act_atbDespesa;


/**
 * Created by eric on 19/09/17.
 */

public class Tab_Despesa1 extends Fragment implements Tela_Despesas {

    private int status_code;
    private shHelper sh;


    //Objeto de requisição padrão
    private JsonObjectRequest requisicao = null;

    private View layout;
    private ListView lista_despesas;
    private List<HashMap<String, String>> lista = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        final AlertDialog.Builder alerta = new AlertDialog.Builder(getContext());

        layout = inflater.inflate(R.layout.tab_despesa1, container, false);

        lista_despesas = (ListView) layout.findViewById(R.id.Lista_Despesas);

        lista_despesas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                String id_despesa = lista.get(position).get("id_despesa").toString();
                String titulo_despesa = lista.get(position).get("titulo_despesa").toString();
                String valor_despesa = lista.get(position).get("valor_despesa").toString();

                sh = new shHelper(getContext());

                sh.salvarDespesa(id_despesa, titulo_despesa, valor_despesa);


                alerta.setMessage(titulo_despesa).setPositiveButton("Abrir", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                }).setNegativeButton("Atribuir", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(Tab_Despesa1.super.getContext(), act_atbDespesa.class);
                        startActivity(intent);
                    }
                });

                AlertDialog alert = alerta.create();
                alert.setTitle("DESPESA: ");
                alert.show();
            }
        });

        carregarDados();

        return layout;
    }



    private void carregarDados(){

        server_URL url = new server_URL(this.getContext());


        sh = new shHelper(this.getContext());

        requisicao = new JsonObjectRequest(Request.Method.GET, url.getURL("despesa")+ "/select", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                String mensagem = " ";

                if (response.has(mensagem)){
                    Snackbar.make(Tab_Despesa1.super.getView(), "Não há despesas cadastradas", Snackbar.LENGTH_SHORT).show();
                }else{
                    try {
                        preencheLista(response.getJSONArray("despesas"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                Log.i("RETORNO: ", response.toString());
                Log.i("CÓDIGO: ", String.valueOf(status_code));


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Erro: ", error.toString());
                String mensagem = "";

                switch (status_code){
                    case 401:
                        mensagem = "E-mail ou senha incorretos, tente novamente !";
                        break;
                    case 500:{
                        mensagem = "Falha na conexão com o servidor: " + error.toString();
                        Snackbar.make(getView(), mensagem + error.toString(), Snackbar.LENGTH_SHORT).show();
                    }
                        break;
                    case 405:
                        mensagem = "Verifique o método de acesso ao servidor (POST, GET)";
                        break;

                }
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + sh.resgatarToken());
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this.getContext());
        requestQueue.add(requisicao);
    }

     private void preencheLista(JSONArray array) throws JSONException {
        ListAdapter adaptador = null;

        try {
            lista = hashMapConverter(array);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (!lista.isEmpty())
        {
            try {

                //SE A LISTA É DIFERENTE DE VAZIO, ADAPTADOR RECEBE OS DADOS DA LISTA
                adaptador = new SimpleAdapter(Tab_Despesa1.super.getContext(), lista, R.layout.modelo_consulta_despesa, new String[]{"titulo_despesa", "desc_despesa", "valor_despesa"}, new int[]{R.id.txtCAMPODESPESA1, R.id.txtCAMPODESPESA2, R.id.txtCAMPODESPESA3});
                lista_despesas.setAdapter(adaptador);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    private List<HashMap<String, String>> hashMapConverter(JSONArray array) throws JSONException{
        List<HashMap<String, String>> despesa = new ArrayList<>();
        int i = 0;

        for (i = 0; i < array.length(); i++){

            HashMap<String, String> dados = new HashMap<String, String>();
            JSONObject dadosObjeto = array.getJSONObject(i);


            dados.put("id_despesa", dadosObjeto.getString("id_despesa"));
            dados.put("titulo_despesa", dadosObjeto.getString("titulo_despesa"));
            dados.put("desc_despesa", dadosObjeto.getString("desc_despesa"));
            dados.put("valor_despesa", dadosObjeto.getString("valor_despesa"));

            despesa.add(dados);
        }
        return  despesa;
    }
}
