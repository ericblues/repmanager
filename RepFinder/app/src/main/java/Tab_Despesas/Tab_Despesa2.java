package Tab_Despesas;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import repfinder.repsys.com.repfinder.R;

/**
 * Created by eric on 19/09/17.
 */

public class Tab_Despesa2 extends Fragment implements Tela_Despesas{

    private View layout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {


        layout = inflater.inflate(R.layout.tab_despesa2, container, false);

        return layout;
    }
}
