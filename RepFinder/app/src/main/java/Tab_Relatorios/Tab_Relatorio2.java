package Tab_Relatorios;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Servidor.server_URL;
import Shared_Preferences.shHelper;
import repfinder.repsys.com.repfinder.R;


/**
 * Created by eric on 10/10/17.
 */

public class Tab_Relatorio2 extends Fragment implements Tela_Relatorio {

    private View layout;

    private TextView totalGeral;
    private TextView organizacao;
    private TextView limpeza;
    private TextView cozinha;
    private TextView lavanderia;
    private TextView manutencao;

    private TextView p_organizacao;
    private TextView p_limpeza;
    private TextView p_cozinha;
    private TextView p_lavanderia;
    private TextView p_manutencao;


    private int status_code;
    private shHelper sh;


    //Objeto de requisição padrão
    private JsonObjectRequest requisicao = null;

    private ListView lista_tarefas;
    private List<HashMap<String, String>> lista = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        layout = inflater.inflate(R.layout.tab_relatorio2, container, false);

        return layout;
    }



    private void carregarDados(){


        server_URL url = new server_URL(this.getContext());


        sh = new shHelper(this.getContext());

        Map<String, String> parametros = new HashMap<>();
        parametros.put("id_republica", "");




        requisicao = new JsonObjectRequest(Request.Method.GET, url.getURL("tarefa") + "/select", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                String mensagem = " ";

                if (response.has(mensagem)){
                    Toast.makeText(getContext(), "Não há tarefas cadastradas", Toast.LENGTH_SHORT).show();
                }else{
                    try {
                        preencheLista(response.getJSONArray("tarefas"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                Log.i("RETORNO: ", response.toString());
                Log.i("CÓDIGO: ", String.valueOf(status_code));


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Erro: ", error.toString());
                String mensagem = "";

                switch (status_code){
                    case 401:
                        mensagem = "E-mail ou senha incorretos, tente novamente !";
                        break;
                    case 500:
                        mensagem = "Falha na conexão com o servidor: " + error.toString();
                        break;
                    case 405:
                        mensagem = "Verifique o método de acesso ao servidor (POST, GET)";
                        break;

                }
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + sh.resgatarToken());
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this.getContext());
        requestQueue.add(requisicao);
    }

    private void preencheLista(JSONArray array) throws JSONException{
        int i = 0;
        int totalOrganizacao = 0;
        float pctOrganizacao = 0;

        int totalLimpeza = 0;
        float pctLimpeza = 0;

        int totalCozinha = 0;
        float pctCozinha = 0;

        int totalLavanderia = 0;
        float pctLavanderia = 0;

        int totalManutencao = 0;
        float pctManutencao = 0;

        try {
            lista = hashMapConverter(array);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (!lista.isEmpty())
        {
            float total = lista.size();

            for (i = 0; i < lista.size(); i++){
                if(lista.get(i).get("tipo_tarefa").equals("Organização"))
                    totalOrganizacao += 1;
                if (lista.get(i).get("tipo_tarefa").equals("Limpeza"))
                    totalLimpeza += 1;
                if (lista.get(i).get("tipo_tarefa").equals("Cozinha"))
                    totalCozinha += 1;
                if (lista.get(i).get("tipo_tarefa").equals("Lavanderia"))
                    totalLavanderia += 1;
                if (lista.get(i).get("tipo_tarefa").equals("Manutenção"))
                    totalManutencao += 1;

            }

            pctOrganizacao = (totalOrganizacao * 100) / total;
            pctLimpeza = (totalLimpeza * 100) / total;
            pctCozinha = (totalCozinha * 100) / total;
            pctLavanderia = (totalLavanderia * 100) / total;
            pctManutencao = (totalManutencao * 100) / total;

            organizacao.setText((""+totalOrganizacao));
            limpeza.setText(""+totalLimpeza);
            cozinha.setText(""+totalCozinha);
            lavanderia.setText(""+totalLavanderia);
            manutencao.setText(""+totalManutencao);

            p_organizacao.setText((""+String.format("%.2f", pctOrganizacao)) + "%");
            p_limpeza.setText(""+String.format("%.2f", pctLimpeza) + "%");
            p_cozinha.setText(""+String.format("%.2f",pctCozinha) + "%");
            p_lavanderia.setText(""+String.format("%.2f",pctLavanderia) + "%");
            p_manutencao.setText(""+String.format("%.2f",pctManutencao) + "%");

        }
    }

    private List<HashMap<String, String>> hashMapConverter(JSONArray array) throws JSONException{
        List<HashMap<String, String>> tarefa = new ArrayList<>();
        int i = 0;

        totalGeral.setText("Total de tarefas: " + array.length());

        for (i = 0; i < array.length(); i++){

            HashMap<String, String> dados = new HashMap<String, String>();
            JSONObject dadosObjeto = array.getJSONObject(i);

            dados.put("id_tarefa", dadosObjeto.getString("id_tarefa"));
            dados.put("titulo_tarefa", dadosObjeto.getString("titulo_tarefa"));
            dados.put("desc_tarefa", dadosObjeto.getString("desc_tarefa"));
            dados.put("tipo_tarefa", dadosObjeto.getString("tipo_tarefa"));

            tarefa.add(dados);
        }
        return  tarefa;
    }




}
