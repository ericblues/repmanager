package Tab_Republica;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Servidor.server_URL;
import Shared_Preferences.shHelper;
import repfinder.repsys.com.repfinder.R;


/**
 * Created by eric on 19/09/17.
 */

public class Tab_Republica1 extends Fragment implements Tela_Republica{




    private int status_code;
    private shHelper sh;


    //Objeto de requisição padrão
    private JsonObjectRequest requisicao = null;


    private View layout;
    private ListView lista_moradores;
    private List<HashMap<String, String>> lista = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        layout = inflater.inflate(R.layout.tab_republica1, container, false);

        lista_moradores = (ListView) layout.findViewById(R.id.Lista_Moradores);

        carregarDados();


        return layout;
    }





     private void carregarDados(){


        server_URL url = new server_URL(this.getContext());


        sh = new shHelper(this.getContext());

        requisicao = new JsonObjectRequest(Request.Method.GET, url.getURL("morador"), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                String mensagem = " ";

                if (response.has(mensagem)){
                    try {
                        Snackbar.make(getView(), response.get("mensagem").toString(), Snackbar.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    try {
                        preencheLista(response.getJSONArray("moradores"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                Log.i("RETORNO: ", response.toString());
                Log.i("CÓDIGO: ", String.valueOf(status_code));


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Erro: ", error.toString());
                String mensagem = "";

                switch (status_code){
                    case 401:
                        mensagem = "E-mail ou senha incorretos, tente novamente !";
                        break;
                    case 500:
                        mensagem = "Falha na conexão com o servidor: " + error.toString();
                        break;
                    case 405:
                        mensagem = "Verifique o método de acesso ao servidor (POST, GET)";
                        break;

                }
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + sh.resgatarToken());
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this.getContext());
        requestQueue.add(requisicao);

    }


    private void preencheLista(JSONArray array) throws JSONException{

        ListAdapter adaptador = null;

        lista = hashMapConverter(array);

        if (!lista.isEmpty())
        {
            try {
                //SE A LISTA É DIFERENTE DE VAZIO, ADAPTADOR RECEBE OS DADOS DA LISTA
                adaptador = new SimpleAdapter(Tab_Republica1.super.getContext(), lista, R.layout.modelo_consulta_tarefa, new String[]{"nome_usuario", "email_usuario"}, new int[]{R.id.txtCAMPO1, R.id.txtCAMPO2});
                lista_moradores.setAdapter(adaptador);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }



    }

    private List<HashMap<String, String>> hashMapConverter(JSONArray array) throws JSONException{
        List<HashMap<String, String>> moradores = new ArrayList<>();
        int i = 0;

        for (i = 0; i < array.length(); i++){

            HashMap<String, String> dados = new HashMap<String, String>();
            JSONObject dadosObjeto = array.getJSONObject(i);


            dados.put("nome_usuario", dadosObjeto.getString("nome_usuario"));
            dados.put("email_usuario", dadosObjeto.getString("email_usuario"));

            moradores.add(dados);
        }
        return  moradores;
    }
}
