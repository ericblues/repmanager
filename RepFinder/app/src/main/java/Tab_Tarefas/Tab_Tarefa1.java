package Tab_Tarefas;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Db_Helper.DbHelper;
import Servidor.server_URL;
import Shared_Preferences.shHelper;
import repfinder.repsys.com.repfinder.R;
import repfinder.repsys.com.repfinder.TabRepublica;
import repfinder.repsys.com.repfinder.act_atbTarefa;

/**
 * Created by eric on 05/09/17.
 */

public class Tab_Tarefa1 extends Fragment implements Tela_Tarefa{



    private DbHelper crud;

    private int status_code;
    private shHelper sh;


    //Objeto de requisição padrão
    private JsonObjectRequest requisicao = null;


    private View layout;
    private ListView lista_tarefas;
    private List<HashMap<String, String>> lista = null;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        final AlertDialog.Builder alerta = new AlertDialog.Builder(getContext());

        layout = inflater.inflate(R.layout.tab_tarefa1, container, false);

        lista_tarefas = (ListView) layout.findViewById(R.id.Lista_Tarefas);


        lista_tarefas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                final String id_tarefa = lista.get(position).get("id_tarefa").toString();
                final String tarefa = lista.get(position).get("titulo_tarefa").toString();
                final String desc_tarefa= lista.get(position).get("desc_tarefa").toString();
                final String tipo_tarefa= lista.get(position).get("tipo_tarefa").toString();

                //Linha a mais
                sh = new shHelper(getContext());

                sh.salvarTarefa(id_tarefa, tarefa, desc_tarefa, tipo_tarefa);

                try{
                    verificarDisponibilidade(id_tarefa);

                }catch (Exception e){
                    Log.i("ERRO: ", e.toString());
                }

                alerta.setMessage(tarefa).setPositiveButton("Abrir", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                }).setNegativeButton("Assumir Tarefa", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(Tab_Tarefa1.super.getContext(), act_atbTarefa.class);
                        startActivity(intent);
                    }
                });

                AlertDialog alert = alerta.create();
                alert.setTitle("TAREFA: ");
                alert.show();

            }
        });


        carregarDados();

        //preencheLista();

        return layout;
    }


    private void carregarDados(){


        server_URL url = new server_URL(this.getContext());


        sh = new shHelper(this.getContext());

        Map<String, String> parametros = new HashMap<>();
        parametros.put("id_republica", "");




        requisicao = new JsonObjectRequest(Request.Method.GET, url.getURL("tarefa") + "/select", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                String mensagem = " ";

                if (response.has(mensagem)){
                    Toast.makeText(getContext(), "Não há tarefas cadastradas", Toast.LENGTH_SHORT).show();
                }else{
                    try {
                        preencheLista(response.getJSONArray("tarefas"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                Log.i("RETORNO: ", response.toString());
                Log.i("CÓDIGO: ", String.valueOf(status_code));


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Erro: ", error.toString());
                String mensagem = "";

                switch (status_code){
                    case 401:
                        mensagem = "E-mail ou senha incorretos, tente novamente !";
                        break;
                    case 500:
                        mensagem = "Falha na conexão com o servidor: " + error.toString();
                        break;
                    case 405:
                        mensagem = "Verifique o método de acesso ao servidor (POST, GET)";
                        break;

                }
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + sh.resgatarToken());
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this.getContext());
        requestQueue.add(requisicao);
    }

    private void preencheLista(JSONArray array) throws JSONException{
        ListAdapter adaptador = null;

        try {
            lista = hashMapConverter(array);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        crud = new DbHelper(getContext());

        //lista = crud.consultaTarefas();

        if (!lista.isEmpty())
        {
            try {
                //SE A LISTA É DIFERENTE DE VAZIO, ADAPTADOR RECEBE OS DADOS DA LISTA
                adaptador = new SimpleAdapter(Tab_Tarefa1.super.getContext(), lista, R.layout.modelo_consulta_despesa, new String[]{"titulo_tarefa_info", "desc_tarefa_info", "tipo_tarefa_info"}, new int[]{R.id.txtCAMPODESPESA1, R.id.txtCAMPODESPESA2, R.id.txtCAMPODESPESA3});
                lista_tarefas.setAdapter(adaptador);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private List<HashMap<String, String>> hashMapConverter(JSONArray array) throws JSONException{
        List<HashMap<String, String>> tarefa = new ArrayList<>();
        int i = 0;

        for (i = 0; i < array.length(); i++){

            HashMap<String, String> dados = new HashMap<String, String>();
            JSONObject dadosObjeto = array.getJSONObject(i);

            dados.put("id_tarefa", dadosObjeto.getString("id_tarefa"));
            dados.put("titulo_tarefa", dadosObjeto.getString("titulo_tarefa"));
            dados.put("titulo_tarefa_info", "Tarefa: " + dadosObjeto.getString("titulo_tarefa"));
            dados.put("desc_tarefa", dadosObjeto.getString("desc_tarefa"));
            dados.put("desc_tarefa_info", "Desc: " + dadosObjeto.getString("desc_tarefa"));
            dados.put("tipo_tarefa", dadosObjeto.getString("tipo_tarefa"));
            dados.put("tipo_tarefa_info", "Tipo: " + dadosObjeto.getString("tipo_tarefa"));

            tarefa.add(dados);
        }
        return  tarefa;
    }




    private void separarDias(JSONArray array) throws JSONException{
        int i = 0;
        int j = 0;
        int diaComparacao = 0;

        String[] arrayDias = new String[7];




        String diasFinal = "0;/0;/0;/0;/0;/0;/0;/";


        arrayDias = diasFinal.split("/");

        diasFinal = "";






        for (i = 0; i < array.length(); i++){
            String[]diasSeparados = array.getJSONObject(i).getString("dias_semana").split(";");


            //if (diaComparacao <= 6){
                for (j = 0; j <= 6; j++){
                    if (diasSeparados[j].equals("1")){
                        if(arrayDias[j].equals("0;")){
                            arrayDias[j] = "1;";
                        }
                        diaComparacao += 1;
                        continue;
                    }
                }
            //}
        }






        for (i = 0; i < arrayDias.length; i++){
             diasFinal += arrayDias[i];
        }


        /*for (i = 0; i < array.length(); i++){

            if(array.getJSONObject(i).getString("dias_semana") == null){
                diasFinal = "0;0;0;0;0;0;0";
                break;
            }
            else{
                dados = array.getJSONObject(i).getString("dias_semana").toString();
                String[] diasSeparados = dados.split(";");
                int j = 0;


                //if (diasSeparados[i].equals("1"))
                //    diasFinal += "1;";
                //else
                //    diasFinal += "0";

                if (diaComparacao == 7)
                    break;
                for (j = diaComparacao; j < diasSeparados.length; j++)
                    if (diasSeparados[j].equals("1")){
                        diasFinal += "1;";
                        arrayDias[diaComparacao] = "1";
                        diaComparacao += 1;
                    }
                    else{
                        arrayDias[diaComparacao] = "0";
                    }

            }
        }*/

        sh.salvarDiasSemana(diasFinal);

        Toast.makeText(getContext(), diasFinal, Toast.LENGTH_SHORT).show();

        diaComparacao = 0;

    }
    //REQUISIÇÃO PARA VERIFICAR DISPONIBILIDADE DA TAREFA

    private void verificarDisponibilidade(String id_tarefa){
        server_URL url = new server_URL(this.getContext());


        sh = new shHelper(this.getContext());




        requisicao = new JsonObjectRequest(Request.Method.GET, url.getURL("disponibilidade") + "/"+id_tarefa, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                String mensagem = " ";

                if (response.has(mensagem)){
                    Toast.makeText(getContext(), "Não há tarefas cadastradas", Toast.LENGTH_SHORT).show();
                }else{

                    Log.i("DIAS ALOCADOS: ", response.toString());
                    try {
                        separarDias(response.getJSONArray("dias"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Dias{{dias},{dias},{dias}}?
                }

                Log.i("RETORNO: ", response.toString());
                Log.i("CÓDIGO: ", String.valueOf(status_code));


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Erro: ", error.toString());
                sh.salvarDiasSemana("0;0;0;0;0;0;0");


                String mensagem = "";

                switch (status_code){
                    case 401:
                        mensagem = "E-mail ou senha incorretos, tente novamente !";
                        break;
                    case 500:
                        mensagem = "Falha na conexão com o servidor: " + error.toString();
                        break;
                    case 405:
                        mensagem = "Verifique o método de acesso ao servidor (POST, GET)";
                        break;

                }
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + sh.resgatarToken());
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this.getContext());
        requestQueue.add(requisicao);
    }

    private void verificaBancoInterno(String id_tarefa){
        crud = new DbHelper(getContext());


    }
}
