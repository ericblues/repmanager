package Tab_Tarefas;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Db_Helper.DbHelper;
import Servidor.server_URL;
import Shared_Preferences.shHelper;
import repfinder.repsys.com.repfinder.R;

/**
 * Created by eric on 05/09/17.
 */

public class Tab_Tarefa2 extends Fragment implements Tela_Tarefa{


    private View layout;
    private DbHelper crud;
    private ListView lista_tarefas_atb;
    private List<HashMap<String, String>> lista = null;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.tab_tarefa2, container, false);

        lista_tarefas_atb = (ListView) layout.findViewById(R.id.Lista_MinhasTarefas);

        preencheLista();

        return layout;
    }

    private void preencheLista(){

        crud = new DbHelper(layout.getContext());
        ListAdapter adaptador = null;
        lista = crud.consultaAtbTarefas();

        if (!lista.isEmpty())
        {
            try {
                //SE A LISTA É DIFERENTE DE VAZIO, ADAPTADOR RECEBE OS DADOS DA LISTA
                adaptador = new SimpleAdapter(Tab_Tarefa2.super.getContext(), lista, R.layout.modelo_consulta_despesa, new String[]{"titulo_tarefa_info", "desc_tarefa_info", "tipo_tarefa_info"}, new int[]{R.id.txtCAMPODESPESA1, R.id.txtCAMPODESPESA2, R.id.txtCAMPODESPESA3});
                lista_tarefas_atb.setAdapter(adaptador);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
