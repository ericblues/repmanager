package broadcastReceiverAux;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import java.util.Calendar;

import Shared_Preferences.shHelper;
import repfinder.repsys.com.repfinder.TabTarefas;

/**
 * Created by eric on 23/10/17.
 */

public class NotificationReceiver extends BroadcastReceiver {

    private shHelper sh;
    @Override
    public void onReceive(Context context, Intent intent) {



        Log.i("Script", "-> Alarme");

        sh = new shHelper(context);


        Log.i("Script ->", "Dia Atual: " + Calendar.getInstance().get(Calendar.DAY_OF_WEEK));

        if (sh.verificaDia(Calendar.getInstance().get(Calendar.DAY_OF_WEEK)).equals("1")){
            gerarNotificacao(context, new Intent(context, TabTarefas.class), "Lembrete República", "A fazer", "Você tem tarefas para fazer hoje");
        }
        else{
            Log.i("Script ->", "Nenhuma tarefa alocada");
        }

    }

    public void gerarNotificacao(Context context, Intent intent, CharSequence ticker, CharSequence titulo, CharSequence descricao){


        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setTicker(ticker)
                .setContentIntent(pendingIntent)
                .setSmallIcon(android.R.drawable.arrow_up_float)
                .setContentTitle(titulo)
                .setContentText(descricao)
                .setAutoCancel(true);


        Notification n = builder.build();
        n.vibrate = new long[]{150, 300, 150, 600};
        n.flags = Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify((int) System.currentTimeMillis(), n);

        try{
            Uri som = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone toque = RingtoneManager.getRingtone(context, som);
            toque.play();
        }catch (Exception e){

        }

    }
}
