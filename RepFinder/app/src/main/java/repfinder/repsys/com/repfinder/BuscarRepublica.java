package repfinder.repsys.com.repfinder;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Servidor.server_URL;
import Shared_Preferences.shHelper;

public class BuscarRepublica extends AppCompatActivity {




    private int status_code;
    private shHelper sh;


    //Objeto de requisição padrão
    private JsonObjectRequest requisicao = null;

    private ListView lista_republica;
    private List<HashMap<String, String>> lista = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_buscar_republica);

        lista_republica = (ListView) findViewById(R.id.Lista_Republicas);

        carregarDados();
    }



    private void carregarDados(){


        server_URL url = new server_URL(getApplicationContext());


        sh = new shHelper(getApplicationContext());

        Map<String, String> parametros = new HashMap<>();
        parametros.put("estado", "SP");
        parametros.put("cidade", "todos");
        parametros.put("tipo_republica", "todos");
        parametros.put("organizacao", "todos");




        requisicao = new JsonObjectRequest(Request.Method.POST, url.getURL("republica"), new JSONObject(parametros), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                String mensagem = " ";

                if (response.has(mensagem)){
                    Toast.makeText(getApplicationContext(), "Não há tarefas cadastradas", Toast.LENGTH_SHORT).show();
                }else{
                    try {
                        preencheLista(response.getJSONArray("republicas"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                Log.i("RETORNO: ", response.toString());
                Log.i("CÓDIGO: ", String.valueOf(status_code));


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Erro: ", error.toString());
                String mensagem = "";

                switch (status_code){
                    case 401:
                        mensagem = "E-mail ou senha incorretos, tente novamente !";
                        break;
                    case 500:
                        mensagem = "Falha na conexão com o servidor: " + error.toString();
                        break;
                    case 405:
                        mensagem = "Verifique o método de acesso ao servidor (POST, GET)";
                        break;

                }
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + sh.resgatarToken());
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(requisicao);
    }

    private void preencheLista(JSONArray array) throws JSONException{
        ListAdapter adaptador = null;

        try {
            lista = hashMapConverter(array);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (!lista.isEmpty())
        {
            try {
                //SE A LISTA É DIFERENTE DE VAZIO, ADAPTADOR RECEBE OS DADOS DA LISTA
                adaptador = new SimpleAdapter(this, lista, R.layout.modelo_consulta_despesa, new String[]{"nome_republica", "qtde_vagas_republica", "valor_aluguel_morador"}, new int[]{R.id.txtCAMPODESPESA1, R.id.txtCAMPODESPESA2, R.id.txtCAMPODESPESA3});
                lista_republica.setAdapter(adaptador);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private List<HashMap<String, String>> hashMapConverter(JSONArray array) throws JSONException{
        List<HashMap<String, String>> republica = new ArrayList<>();
        int i = 0;

        for (i = 0; i < array.length(); i++){

            HashMap<String, String> dados = new HashMap<String, String>();
            JSONObject dadosObjeto = array.getJSONObject(i).getJSONObject("dados");

            dados.put("id_republica", dadosObjeto.getString("id_republica"));
            dados.put("nome_republica", dadosObjeto.getString("nome_republica"));




            dados.put("qtde_vagas_republica", "Qtde Vagas: " + dadosObjeto.getString("qtde_vagas_republica"));
            dados.put("valor_aluguel_morador", "Valor por pessoa: " + dadosObjeto.getString("valor_aluguel_morador"));

            republica.add(dados);
        }
        return  republica;
    }

}
