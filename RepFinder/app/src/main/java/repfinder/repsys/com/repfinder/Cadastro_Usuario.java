package repfinder.repsys.com.repfinder;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import Servidor.server_URL;
import Shared_Preferences.shHelper;

public class Cadastro_Usuario extends AppCompatActivity {

    //CODIGO DE RESPOSTA DO SERVIDOR
    private int status_code;

    //Shared Preferences
    private shHelper sh;

    private Button btnCadastrar;
    private EditText edtSenha;
    private EditText edtNome;
    private EditText edtSNome;
    private EditText edtEmail;
    private int click = 0;


    //Objeto de requisição padrão
    private JsonObjectRequest requisicao = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_cadastro__usuario);

        edtSenha = (EditText) findViewById(R.id.edtSenha);
        edtNome = (EditText) findViewById(R.id.edtNome_Usuario);
        edtSNome = (EditText) findViewById(R.id.edtSobreNome_Usuario);
        edtEmail = (EditText) findViewById(R.id.edtEmail);


        btnCadastrar = (Button) findViewById(R.id.btnCadastrarMe);

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastrar();
            }
        });



        edtSenha.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });

        edtSenha.setTextIsSelectable(false);
        edtSenha.setLongClickable(false);



        edtSenha.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //final int DRAWABLE_LEFT = 0;
                //final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                //final int DRAWABLE_BOTTOM = 3;

                Drawable novoIcone;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (edtSenha.getRight() - edtSenha.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (click == 1){
                            novoIcone = getResources().getDrawable(R.drawable.ic_eye_off_black_18dp);
                            edtSenha.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            edtSenha.setCompoundDrawablesWithIntrinsicBounds(null, null, novoIcone, null);
                            edtSenha.setSelection(edtSenha.length());
                            click = 0;
                        }else{
                            novoIcone = getResources().getDrawable(R.drawable.ic_eye_black_18dp);
                            edtSenha.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            edtSenha.setCompoundDrawablesWithIntrinsicBounds(null, null, novoIcone, null);
                            edtSenha.setSelection(edtSenha.length());
                            click = 1;
                        }
                        return true;
                    }
                }
                return false;
            }
        });

    }

    private void cadastrar(){
        server_URL url = new server_URL(getApplicationContext());


        Map<String, String> parametros = new HashMap<>();
        parametros.put("nome_usuario", edtNome.getText().toString());
        parametros.put("sobrenome_usuario", edtSNome.getText().toString());
        parametros.put("email_usuario", edtEmail.getText().toString());
        parametros.put("senha_usuario", edtSenha.getText().toString());

        requisicao = new JsonObjectRequest(Request.Method.POST, url.getURL("cadastro"), new JSONObject(parametros), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                String mensagem = " ";

               if (response.has("mensagem"))
                   try {
                       mensagem = response.getString("mensagem");
                   } catch (JSONException e) {
                       e.printStackTrace();
                   }

                Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_LONG).show();
                Log.i("RETORNO: ", response.toString());
                Log.i("CÓDIGO: ", String.valueOf(status_code));
                
                if (status_code == 200)
                    Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_SHORT);
                if (status_code == 400)
                    Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Erro: ", error.toString());
                String mensagem = "";

                switch (status_code){
                    case 401:
                        mensagem = "E-mail ou senha incorretos, tente novamente !";
                        break;
                    case 500:
                        mensagem = "Falha na conexão com o servidor: " + error.toString();
                        break;
                    case 405:
                        mensagem = "Verifique o método de acesso ao servidor (POST, GET)";
                        break;

                }

                Toast.makeText(getApplicationContext(), mensagem + status_code, Toast.LENGTH_SHORT).show();



                //OUTRA MANEIRA DE PEGAR O STATUS CODE, PORÉM, APENAS DE ERRO
                //int codigo = error.networkResponse.statusCode;
            }
        }){
            //Método para pegar o status code
            //200 = sucesso
            //401 - credencial invalida
            //500 - falha de conexão com o servidor
            //405 - metodo errado, post ao inves de get ou vice versa
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }

            //Método que envia o header para o servidor
            //será necessário para enviar o token que permite as operações
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{

                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(requisicao);
    }
}
