package repfinder.repsys.com.repfinder;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import Shared_Preferences.shHelper;

public class Desenvolvedor extends AppCompatActivity {


    private Button btnAtualizarIP;
    private EditText edtIP;
    private shHelper sh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_desenvolvedor);


        edtIP = (EditText) findViewById(R.id.edtIP) ;
        btnAtualizarIP = (Button) findViewById(R.id.btnAtualizarIP);
        sh = new shHelper(getApplicationContext());

        btnAtualizarIP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sh.salvarIP(edtIP.getText().toString());

                Toast.makeText(getApplicationContext(), "IP ATUALIZADO PARA: " + edtIP.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
