package repfinder.repsys.com.repfinder;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import Servidor.server_URL;
import Shared_Preferences.shHelper;

public class Login extends AppCompatActivity {


    //CODIGO DE RESPOSTA DO SERVIDOR
    private int status_code;

    //Shared Preferences
    private shHelper sh;



    //APENAS PARA TROCAR O IP DO SERVIDOR CASO SEJA NECESSÁRIO
    private TextView desenvolvedor;

    private EditText txtEMAIL;
    private EditText txtSENHA;
    private TextView txtCadastrar;
    private Button btnLogar;
    private EditText senha;
    private int click = 0;

    //Objeto de requisição padrão
    private JsonObjectRequest requisicao = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_login);

        btnLogar = (Button) findViewById(R.id.btnLogar);
        senha = (EditText) findViewById(R.id.txtSenhaLogin);
        txtCadastrar = (TextView) findViewById(R.id.txtCadastrar);
        txtEMAIL = (EditText) findViewById(R.id.txtEmailLogin);
        txtSENHA = (EditText) findViewById(R.id.txtSenhaLogin);

        //APENAS PARA TROCAR O IP DO SERVIDOR CASO SEJA NECESSÁRIO
        desenvolvedor = (TextView) findViewById(R.id.txtDesenvolvedor);

        desenvolvedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, Desenvolvedor.class);
                startActivity(intent);
            }
        });

        txtCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, Cadastro_Usuario.class);
                startActivity(intent);
            }
        });

        btnLogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Intent intent = new Intent(Login.this, Menu_Principal.class);
                //startActivity(intent);

                logar();
            }
        });

        senha.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });

        senha.setTextIsSelectable(false);
        senha.setLongClickable(false);


        senha.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                Drawable novoIcone;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (senha.getRight() - senha.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (click == 1){
                            novoIcone = getResources().getDrawable(R.drawable.ic_eye_off_black_18dp);
                            senha.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            senha.setCompoundDrawablesWithIntrinsicBounds(null, null, novoIcone, null);
                            senha.setSelection(senha.length());
                            click = 0;
                        }else{
                            novoIcone = getResources().getDrawable(R.drawable.ic_eye_black_18dp);
                            senha.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            senha.setCompoundDrawablesWithIntrinsicBounds(null, null, novoIcone, null);
                            senha.setSelection(senha.length());
                            click = 1;
                        }
                        return true;
                    }
                }
                return false;
            }
        });
    }

    private void logar(){
        //Teste de comentario para novo commit
        //objeto da classe server url, que vai pegar a url do servidor
        //o parâmetro esperado para o método getURL é "login"
        server_URL url = new server_URL(getApplicationContext());

        final Map<String, String> parametros = new HashMap<>();
        parametros.put("email_usuario", txtEMAIL.getText().toString());
        parametros.put("senha_usuario", txtSENHA.getText().toString());

        sh = new shHelper(getApplicationContext());

        requisicao = new JsonObjectRequest(Request.Method.POST, url.getURL("login"), new JSONObject(parametros), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_LONG).show();
                Log.i("RETORNO: ", response.toString());
                try {
                    sh.salvarToken(response.getString("token"));
                    carregarDadosUsuario();
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "Falha no token: " + e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Codigo do erro: ", status_code + " ");
                Log.i("Erro: ", error.toString());

                String mensagem = "";

                switch (status_code){
                    case 401:
                        mensagem = "E-mail ou senha incorretos, tente novamente !";
                        break;
                    case 500:
                        mensagem = "Falha na conexão com o servidor: " + error.toString();
                        break;
                    case 405:
                        mensagem = "Verifique o método de acesso ao servidor (POST, GET)";
                        break;

                }

                Toast.makeText(getApplicationContext(), error.toString() + "CODIGO: " + status_code, Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }

            //Método que envia o header para o servidor
            //será necessário para enviar o token que permite as operações
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{

                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(requisicao);

    }

    private void carregarDadosUsuario(){

        server_URL url = new server_URL(getApplicationContext());

        sh = new shHelper(getApplicationContext());

        requisicao = new JsonObjectRequest(Request.Method.GET, url.getURL("carregarDados"), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("Dados do Usuario => ", response.toString());

                try {
                    separarArray(response);
                } catch (JSONException e) {
                    Log.i("Erro: ", e.toString());
                    e.printStackTrace();
                }
                Intent intent = new Intent(Login.this, Menu_Principal.class);
                startActivity(intent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Erro: ", error.toString());
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                sh = new shHelper(getApplicationContext());

                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + sh.resgatarToken());
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(requisicao);

    }

    private void separarArray(JSONObject obj) throws JSONException{

        JSONObject dados = obj.getJSONObject("dados");

        JSONObject usuario = null;
        JSONObject republica = null;

        try{
            usuario = dados.getJSONObject("dadosUsuario");

            if (!usuario.getString("tipo_usuario").equals("0")){
                republica = dados.getJSONObject("dadosRepublica");
                sh.salvarDadosUsuario(usuario, republica);
            }else{
                sh.salvarDadosUsuario(usuario, null);
            }

            Log.i("Usuario", dados.getJSONObject("dadosUsuario").toString());
            Log.i("Republica", dados.getJSONObject("dadosRepublica").toString());
        }catch (JSONException e){
            Log.i("Erro: ", e.toString());
            e.printStackTrace();
        }
    }
}
