package repfinder.repsys.com.repfinder;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.ViewFlipper;

import org.w3c.dom.Text;

import java.util.concurrent.ExecutionException;

import Shared_Preferences.shHelper;

public class Menu_Principal extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //Shared Preferences
    private shHelper sh;

    private ViewFlipper vf;

    private TextView nome_usuario;
    private TextView email_usuario;
    private TextView republica_usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_menu__principal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        vf = (ViewFlipper) findViewById(R.id.vfTeste);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu__principal, menu);

        sh = new shHelper(getApplicationContext());

        nome_usuario = (TextView) findViewById(R.id.txtNome_Usuario);
        email_usuario = (TextView) findViewById(R.id.txtEmail_Usuario);
        republica_usuario = (TextView) findViewById(R.id.txtRepublica_Usuario);

        nome_usuario.setText(sh.resgatarCampo("nome_usuario"));
        email_usuario.setText(sh.resgatarCampo("email_usuario"));

        try{
            republica_usuario.setText(sh.resgatarCampo("nome_republica"));
        }catch (Exception e){
            Log.i("Erro: ", e.toString());
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_inicio) {
            vf.setDisplayedChild(0);
        } else if (id == R.id.nav_republica) {

            //Intent intent = new Intent(Menu_Principal.this, Republicas_Inicial.class);
            //startActivity(intent);

            Intent intent = new Intent(Menu_Principal.this, TabRepublica.class);
            startActivity(intent);

            //vf.setDisplayedChild(0);
        } else if (id == R.id.nav_tarefas) {
            Intent intent = new Intent(Menu_Principal.this, TabTarefas.class);
            startActivity(intent);
        } else if (id == R.id.nav_despesas) {
            Intent intent = new Intent(Menu_Principal.this, TabDespesas.class);
            startActivity(intent);
        } else if (id == R.id.nav_relatorios) {
            //Intent intent = new Intent(Menu_Principal.this, TabRelatorios.class);
            //startActivity(intent);


            Intent intent = new Intent(Menu_Principal.this, SubMenuRelatorios.class);
            startActivity(intent);
        } else if (id == R.id.nav_habilidades) {

        }else if (id == R.id.nav_buscarRepublica){
            Intent intent = new Intent(Menu_Principal.this, BuscarRepublica.class);
            startActivity(intent);
        }else if (id == R.id.nav_configuracoes){
            //Testando tela de relatorio de desempenho

            Intent intent = new Intent(Menu_Principal.this, RelatorioDesempenho.class);
            startActivity(intent);

        }else if (id == R.id.nav_sair){
            sh = new shHelper(getApplicationContext());

            sh.limparToken();

            Intent intent = new Intent(Menu_Principal.this, Login.class);
            startActivity(intent);
        }else if (id == R.id.nav_cadRepublica){
            Intent intent = new Intent(Menu_Principal.this, Republicas_Inicial.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
