package repfinder.repsys.com.repfinder;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import Tab_Tarefas.Tab_Tarefa1;

/**
 * Created by eric on 27/09/17.
 */

public class MessagingService extends FirebaseMessagingService {

    JSONObject json = null;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //RemoteMessage.Notification notification = remoteMessage.getNotification();


        try {
            json = new JSONObject(remoteMessage.getData().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("TESTE JSON: " + json.toString());
        mostrarNotificacao();


        Log.i("RECEBEU ", "");
        if (remoteMessage.getData().size() > 0){
            Log.i("teste: ", remoteMessage.getData().toString());
            try{
                //JSONObject json = new JSONObject(remoteMessage.getData().toString());
                Log.d("RECEBEU: ", json.toString());

            }catch (Exception e){
                Log.d("ERRO AO RECEBER: ", e.toString());
            }
        }

    }

    public void mostrarNotificacao(){


        //Log.i("NOTIFICACAO: ", notification.getBody());



        String titulo = "";
        String message = "";

        try {
            System.out.println("DATA: " + json.toString());

            titulo = json.getString("title");
            message = json.getString("message");

            if (message.equals("1"))
                message = "Nova Tarefa cadastrada";
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(this, Tab_Tarefa1.class);
        PendingIntent pendingIntent = PendingIntent
                .getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        Notification notificacao = builder
                .setSmallIcon(R.drawable.bebida_escuro)
                .setContentTitle(titulo)
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificacao);
    }
}
