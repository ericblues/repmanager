package repfinder.repsys.com.repfinder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PrimeroAcesso extends AppCompatActivity {


    private Button cadastrar;
    private Button procurar;
    private Button editar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_primero_acesso);

        cadastrar = (Button) findViewById(R.id.btnCadastrarNovaRepublica);
        procurar = (Button) findViewById(R.id.btnBuscarRepublica);


        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    finish();
                    Intent intent = new Intent(PrimeroAcesso.this, Republicas_Inicial.class);
                    startActivity(intent);
            }
        });
    }
}
