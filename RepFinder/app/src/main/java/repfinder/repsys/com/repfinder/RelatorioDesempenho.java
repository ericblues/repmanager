package repfinder.repsys.com.repfinder;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Tab_Tarefas.Tab_Tarefa1;

public class RelatorioDesempenho extends AppCompatActivity {

    private Spinner spnTiposTarefa;

    private ListView listaMoradores;


    private List<HashMap<String, String>> lista = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_relatorio_desempenho);

        spnTiposTarefa = (Spinner) findViewById(R.id.spnHabilidades);

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.valoresTipoTareafa));
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnTiposTarefa.setAdapter(adaptador);


        listaMoradores = (ListView) findViewById(R.id.Lista_Desempenho);

        try {
            lista = hashMapConverter();
        } catch (Exception e) {
            e.printStackTrace();
        }


        ListAdapter adaptadorLista = null;


        if (!lista.isEmpty())
        {
            try {
                //SE A LISTA É DIFERENTE DE VAZIO, ADAPTADOR RECEBE OS DADOS DA LISTA
                adaptadorLista = new SimpleAdapter(this, lista, R.layout.modelo_consulta_tarefa, new String[]{"nome", "pct"}, new int[]{R.id.txtCAMPO1, R.id.txtCAMPO2});
                listaMoradores.setAdapter(adaptadorLista);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }





    private List<HashMap<String, String>> hashMapConverter(){
        List<HashMap<String, String>> morador = new ArrayList<>();
        int i = 0;

        for (i = 0; i < 5; i++){

            HashMap<String, String> dados = new HashMap<String, String>();

            dados.put("nome", "Nome: " + "Usuario " + (i+1));
            dados.put("pct", "Aprovação: " + "100%");
            dados.put("desempenho", "Nível: " + "ALTO");


            morador.add(dados);
        }
        return  morador;
    }

}
