package repfinder.repsys.com.repfinder;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Endereco.Adress;
import Endereco.Adress_Request;
import Servidor.server_URL;
import Shared_Preferences.shHelper;

public class Republicas_Inicial extends AppCompatActivity {



    private int status_code;
    private shHelper sh;

    //Objeto de requisição padrão
    private JsonObjectRequest requisicao = null;


    //Botões
    private Button avancar = null;
    private Button retroceder = null;

    //Spinner
    private Spinner spnESTADO;
    private Spinner spnTipoRepublica;

    /*  CÓDIGO PARA COLOCAR NA TELA DE EDIÇÃO DE DADOS EXTRAS DA REPÚBLICA
        private Spinner spnBanheiros;
        private Spinner spnGaragens;
        private Spinner spnCozinhas;
        private Spinner spnLavanderias;
        private Spinner spnSalas;
        private Spinner spnQuartos;*/

    //ViewFLipper
    private ViewFlipper vf = null;

    //Pega numero do layout atual
    private int telaApresentada;

    //EDIT TEXTS

    private EditText edtNomeRep;
    private EditText edtValorAluguel;
    private EditText edtQtdeVagas;


    private EditText edtCEP;
    private EditText edtRUA;
    private EditText edtNUMERO;
    private EditText edtBAIRRO;
    private EditText edtCOMPLEMENTO;
    private EditText edtCIDADE;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_republicas__inicial);


        //Refêrenciando as EditTexts
        edtNomeRep = (EditText) findViewById(R.id.edtNomeRepublica);
        edtValorAluguel = (EditText) findViewById(R.id.edtValorAluguel);
        edtQtdeVagas = (EditText) findViewById(R.id.edtQtdeVagas);

        edtCEP = (EditText) findViewById(R.id.edtCEP);
        edtRUA = (EditText) findViewById(R.id.edtRUA);
        edtNUMERO = (EditText) findViewById(R.id.edtNUMERO);
        edtBAIRRO = (EditText) findViewById(R.id.edtBAIRRO);
        edtCOMPLEMENTO = (EditText) findViewById(R.id.edtCOMPLEMENTO);
        edtCIDADE = (EditText) findViewById(R.id.edtCIDADE);

        edtCEP.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String working = s.toString();

                if (s.length() == 5 && before == 0){
                    working += "-";
                    edtCEP.setText(working);
                    edtCEP.setSelection(working.length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 9){
                    consultaCEP();
                }
            }
        });



        vf = (ViewFlipper) findViewById(R.id.vfRepublicas);
        avancar = (Button) findViewById(R.id.btnAvancarRepublica);
        retroceder = (Button) findViewById(R.id.btnRetrocederRepublica);


        spnESTADO = (Spinner) findViewById(R.id.spnESTADO);


        spnTipoRepublica = (Spinner) findViewById(R.id.spnTipoRepublica);

        /* CÓDIGO PARA COLOCAR NA TELA DE EDIÇÃO DE DADOS EXTRAS DA REPÚBLICA
        spnBanheiros = (Spinner) findViewById(R.id.spnBanheiros);
        spnGaragens = (Spinner) findViewById(R.id.spnGaragens);
        spnCozinhas = (Spinner) findViewById(R.id.spnCozinhas);
        spnLavanderias = (Spinner) findViewById(R.id.spnLavanderias);
        spnSalas = (Spinner) findViewById(R.id.spnSalas);
        spnQuartos = (Spinner) findViewById(R.id.spnQuartos);


        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.valores_Comodos));
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnBanheiros.setAdapter(adaptador);
        spnGaragens.setAdapter(adaptador);
        spnCozinhas.setAdapter(adaptador);
        spnLavanderias.setAdapter(adaptador);
        spnSalas.setAdapter(adaptador);
        spnQuartos.setAdapter(adaptador);*/


        ArrayAdapter<String> adaptador2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.valoresTipoRepublica));
        adaptador2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnTipoRepublica.setAdapter(adaptador2);



        ArrayAdapter<String> adaptadorEstado = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.valoresEstados));
        adaptadorEstado.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnESTADO.setAdapter(adaptadorEstado);

        spnESTADO.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Snackbar.make(view, spnESTADO.getItemAtPosition(position).toString(), Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        avancar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                telaApresentada = vf.getDisplayedChild();

                switch (telaApresentada){
                    case 0:
                        vf.setDisplayedChild(1);
                        break;
                    case 1:
                        conferirDados();
                        avancar.setText("Cadastrar");
                        vf.setDisplayedChild(2);
                        break;
                    case 2:
                        cadastrarRepublica();
                        break;
                    default:
                        break;
                }
            }
        });

        retroceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                telaApresentada = vf.getDisplayedChild();

                switch (telaApresentada){
                    case 1:
                        vf.setDisplayedChild(0);
                        break;
                    case  2:
                        vf.setDisplayedChild(1);
                        break;
                    //case 3:
                    //   vf.setDisplayedChild(2);
                    //    break;
                    default:
                        break;
                }
            }
        });




    }

    private void preencheCampos(JSONObject dados) throws JSONException{

        edtRUA.setText(dados.getString("logradouro"));
        edtBAIRRO.setText(dados.getString("bairro"));
        edtCOMPLEMENTO.setText(dados.getString("complemento"));
        edtCIDADE.setText(dados.getString("localidade"));


        ArrayAdapter<String> adaptadorEstado = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.valoresEstados));
        adaptadorEstado.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnESTADO.setAdapter(adaptadorEstado);

        spnESTADO.setSelection(adaptadorEstado.getPosition(dados.getString("uf")));
    }

    private void consultaCEP(){
        String URL = "https://viacep.com.br/ws/"+edtCEP.getText().toString()+"/json/";

        requisicao = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.has("erro")){
                        Log.i("Erro: ", response.toString());
                        Toast.makeText(getApplicationContext(), "CEP inválido", Toast.LENGTH_SHORT).show();
                    }else{
                        preencheCampos(response);
                        Toast.makeText(getApplicationContext(), "Dados Econtrados para o CEP", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Erro: ", error.toString());
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(requisicao);
    }

    private void cadastrarRepublica(){

        //objeto da classe server url, que vai pegar a url do servidor
        //o parâmetro esperado para o método getURL é "login"
        server_URL url = new server_URL(getApplicationContext());

        Map<String, String> parametros = new HashMap<>();
        parametros.put("nome_republica", edtNomeRep.getText().toString());
        parametros.put("valor_aluguel_republica", edtValorAluguel.getText().toString());
        parametros.put("qdte_vagas_republica", edtQtdeVagas.getText().toString());
        parametros.put("tipo_republica", spnTipoRepublica.getSelectedItem().toString());
        parametros.put("cep_endereco", edtCEP.getText().toString());
        parametros.put("rua_endereco", edtRUA.getText().toString());
        parametros.put("numero_endereco", edtNUMERO.getText().toString());
        parametros.put("complemento_endereco", edtCOMPLEMENTO.getText().toString());
        parametros.put("bairro_endereco", edtBAIRRO.getText().toString());
        parametros.put("cidade_endereco", edtCIDADE.getText().toString());
        parametros.put("estado_endereco", spnESTADO.getSelectedItem().toString());



        sh = new shHelper(getApplicationContext());

        requisicao = new JsonObjectRequest(Request.Method.POST, url.getURL("cadastroRepublica"), new JSONObject(parametros), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                String mensagem = " ";

                if (response.has("mensagem"))
                    try {
                        mensagem = response.getString("mensagem");
                        Toast.makeText(getApplicationContext(), mensagem.toString(), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                if (status_code == 200){
                    Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_SHORT).show();

                    finish();
                    Intent intent = new Intent(Republicas_Inicial.this, Menu_Principal.class);
                    startActivity(intent);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Erro: ", error.toString());
                String mensagem = "";

                switch (status_code){
                    case 401:
                        mensagem = "E-mail ou senha incorretos, tente novamente !";
                        break;
                    case 500:
                        mensagem = "Falha na conexão com o servidor: " + error.toString();
                        break;
                    case 405:
                        mensagem = "Verifique o método de acesso ao servidor (POST, GET)";
                        break;

                }

                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        }){
            //Método para pegar o status code
            //200 = sucesso
            //401 - credencial invalida
            //500 - falha de conexão com o servidor
            //405 - metodo errado, post ao inves de get ou vice versa
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }

            //Método que envia o header para o servidor
            //será necessário para enviar o token que permite as operações
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                sh = new shHelper(getApplicationContext());

                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + sh.resgatarToken());
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(requisicao);


    }

    private void conferirDados(){


        TextView txtNomeRep = (TextView) findViewById(R.id.txtNome_Republica);
        TextView txtValorALuguel = (TextView) findViewById(R.id.txtValorAluguel);
        TextView txtQtdeVagas = (TextView) findViewById(R.id.txtQtdeVagas);
        TextView txtTipoRepublica = (TextView) findViewById(R.id.txtTipoRepublica);

        TextView txtCEP = (TextView) findViewById(R.id.txtCEP);
        TextView txtRUA = (TextView) findViewById(R.id.txtRua);
        TextView txtNUMERO = (TextView) findViewById(R.id.txtNumeroRepublica);
        TextView txtBAIRRO = (TextView) findViewById(R.id.txtBairro);
        TextView txtCOMPLEMENTO = (TextView) findViewById(R.id.txtComplemento);
        TextView txtCIDADE = (TextView) findViewById(R.id.txtCidade);
        TextView txtESTADO = (TextView) findViewById(R.id.txtEstado);




        String strNomeRep = "Nome: " + edtNomeRep.getText().toString();
        String strValorALuguel = "Valor do Aluguel: " + edtValorAluguel.getText().toString();
        String strQtdeVagas = "Quantidade de Vagas: " + edtQtdeVagas.getText().toString();
        String strTipoRepublica = "Tipo de República: " + spnTipoRepublica.getSelectedItem().toString();
        String strCEP = "CEP: " + edtCEP.getText().toString();
        String strRUA = "Rua: " + edtRUA.getText().toString();
        String strBAIRRO = "Bairro: " + edtBAIRRO.getText().toString();
        String strNUMERO = "Número: " + edtNUMERO.getText().toString();
        String strCOMPLEMENTO = "Complemento: " + edtCOMPLEMENTO.getText().toString();
        String strCIDADE = "Cidade: " + edtCIDADE.getText().toString();
        String strESTADO = "Estado: " + spnESTADO.getSelectedItem().toString();

        txtNomeRep.setText(strNomeRep);
        txtValorALuguel.setText(strValorALuguel);
        txtQtdeVagas.setText(strQtdeVagas);
        txtTipoRepublica.setText(strTipoRepublica);
        txtCEP.setText(strCEP);
        txtRUA.setText(strRUA);
        txtBAIRRO.setText(strBAIRRO);
        txtNUMERO.setText(strNUMERO);
        txtCOMPLEMENTO.setText(strCOMPLEMENTO);
        txtCIDADE.setText(strCIDADE);
        txtESTADO.setText(strESTADO);
    }


    private void filtrarCidades(){

    }
}
