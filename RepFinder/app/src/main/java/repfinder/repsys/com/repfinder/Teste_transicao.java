package repfinder.repsys.com.repfinder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import Shared_Preferences.shHelper;
import agency.tango.materialintroscreen.MessageButtonBehaviour;
import agency.tango.materialintroscreen.SlideFragmentBuilder;

import agency.tango.materialintroscreen.MaterialIntroActivity;

public class Teste_transicao extends MaterialIntroActivity {

    private shHelper sh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addSlide(new SlideFragmentBuilder().backgroundColor(R.color.colorPrimaryDark)
                .buttonsColor(R.color.colorAccent)
                .title("RepSys")
                .description(getString(R.string.Texto_Apresentacao))
                .image(R.drawable.casinha)
                .build(), new MessageButtonBehaviour(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Teste_transicao.this, Login.class);
                startActivity(intent);
            }
        }, "Pular introdução"));


        addSlide(new SlideFragmentBuilder().backgroundColor(R.color.colorPrimaryDark)
                .buttonsColor(R.color.accent)
                .title("Procurando uma república para morar?")
                .description("Basta pesquisar o estado e cidade de sua preferência no nosso sistema irá te mostrar repúblicas disponíveis")
                .image(R.drawable.em_andamento)
                .build(), new MessageButtonBehaviour(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Replace action
            }
        }, "Pesquisar Vagas"));

        addSlide(new SlideFragmentBuilder().backgroundColor(R.color.colorPrimaryDark)
                .buttonsColor(R.color.accent)
                .title("Quer dividir o aluguel com outros estudantes?")
                .description("Aqui você pode cadastrar sua república rapidamente e anunciá-la, e logo pessoas interessadas poderam solicitar morar com você")
                .image(R.drawable.em_andamento)
                .build(), new MessageButtonBehaviour(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Replace action
            }
        }, "Cadastrar República"));


        addSlide(new SlideFragmentBuilder().backgroundColor(R.color.colorPrimaryDark)
                        .buttonsColor(R.color.accent)
                        .title("Prático e Rápido")
                        .description("Organiza as tarefas e despesas da república em um só lugar. Comece a usar o app e usufrua do melhor da praticidade")
                        .image(R.drawable.em_andamento)
                        .build());

    }

    @Override
    protected void onResume(){
        super.onResume();

        sh = new shHelper(getApplicationContext());

        String token = sh.resgatarToken();


        if (token != null){
            Intent intent = new Intent(Teste_transicao.this, Menu_Principal.class);
            startActivity(intent);
        }
    }
}
