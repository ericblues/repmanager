package repfinder.repsys.com.repfinder;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Servidor.server_URL;
import Shared_Preferences.shHelper;


public class act_atbDespesa extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{


    private List<HashMap<String, String>> dadosMoradores = new ArrayList<>();


    private shHelper sh;


    //Objeto de requisição padrão
    private JsonObjectRequest requisicao = null;

    private int status_code;


    private Calendar calendar;
    private TextView dateView;
    private int year, month, day;


    private EditText edtDataVencimento;
    private TextView despesaSelecionada;
    private Spinner spnRepetir;
    private Spinner spnMorador;
    private Button btnAtribuir;
    private Button cancelar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_atb_despesa);

        despesaSelecionada = (TextView) findViewById(R.id.txtDespesaSelecionada);
        edtDataVencimento = (EditText) findViewById(R.id.edtDataVencimento);
        spnRepetir = (Spinner) findViewById(R.id.spnRepetirDespesa);
        spnMorador = (Spinner) findViewById(R.id.spnMoradorDespesa);

        btnAtribuir = (Button) findViewById(R.id.btnCadastrarAtbDespesa);
        cancelar = (Button) findViewById(R.id.btnCancelarAtbDespesa);

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        ArrayAdapter<String> adaptadorNomesTeste = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.valoresNomesTeste));
        adaptadorNomesTeste.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnMorador.setAdapter(adaptadorNomesTeste);

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.valoresRepeticaoDespesa));
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnRepetir.setAdapter(adaptador);


        dateView = (TextView) findViewById(R.id.txtData);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        //Atribui o nome da tarefa
        sh = new shHelper(getApplicationContext());
        despesaSelecionada.setText(sh.resgatarDespesa(2));




        carregarDados();
        edtDataVencimento.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (edtDataVencimento.getRight() - edtDataVencimento.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                       datePicker(v);
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar cal = new GregorianCalendar(year, month, day);
        edtDataVencimento.setText(view.getDayOfMonth() + "/" + view.getMonth() + "/" + view.getYear());
    }

    public void setDate(Calendar date) {
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);

    }

    public void datePicker(View view){

        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getSupportFragmentManager(), "Data Picker");

    }



    private void preencherSpinner(JSONArray array) throws JSONException {


        ArrayList<String> lista = new ArrayList<>();
        int i = 0;

        for (i = 0; i < array.length(); i++)
            lista.add(i, array.getJSONObject(i).getString("nome_usuario"));

        for (i = 0; i< array.length(); i++){

            HashMap<String, String> dados = new HashMap<String, String>();
            JSONObject dadosObjeto = array.getJSONObject(i);


            dados.put("id_usuario", dadosObjeto.getString("id_usuario"));
            dados.put("email_usuario", dadosObjeto.getString("email_usuario"));
            dados.put("nome_usuario", dadosObjeto.getString("nome_usuario"));

            dadosMoradores.add(dados);
        }

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, lista);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnMorador.setAdapter(adaptador);

    }



    private void carregarDados(){


        server_URL url = new server_URL(getApplicationContext());


        sh = new shHelper(getApplicationContext());

        requisicao = new JsonObjectRequest(Request.Method.GET, url.getURL("morador"), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    preencherSpinner(response.getJSONArray("moradores"));
                }catch (Exception e){
                    Log.i("ERRO NA FUNÇÃO: ", e.toString());
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }


                Log.i("RETORNO: ", response.toString());
                Log.i("CÓDIGO: ", String.valueOf(status_code));


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Erro: ", error.toString() + "Status Code: " + status_code);
                String mensagem = "";

            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + sh.resgatarToken());
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(requisicao);



    }


    private void atribuir(){
        server_URL url = new server_URL(getApplicationContext());

        Map<String, String> parametros = new HashMap<>();
        //parametros.put("id_usuario", id);
        parametros.put("id_despesa", sh.resgatarDespesa(1));
        //parametros.put("repeticao", spnRepeticao.getSelectedItem().toString());
        //parametros.put("Valor", " ");


        sh = new shHelper(getApplicationContext());

        requisicao = new JsonObjectRequest(Request.Method.POST, url.getURL("atbDespesa"), new JSONObject(parametros), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                String mensagem = " ";

                if (response.has("mensagem"))
                    try {
                        mensagem = response.getString("mensagem");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                //if (status_code == 200)
                //    gerarAlarme();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Erro: ", error.toString());
                String mensagem = "";

                switch (status_code){
                    case 401:
                        mensagem = "E-mail ou senha incorretos, tente novamente !";
                        break;
                    case 500:
                        mensagem = "Falha na conexão com o servidor: " + error.toString();
                        break;
                    case 405:
                        mensagem = "Verifique o método de acesso ao servidor (POST, GET)";
                        break;

                }

                Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                sh = new shHelper(getApplicationContext());

                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + sh.resgatarToken());
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(requisicao);
    }


}
