package repfinder.repsys.com.repfinder;

import Db_Helper.DbHelper;
import TimerPicker.TimerPickerFragment;

import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Servidor.server_URL;
import Shared_Preferences.shHelper;

public class act_atbTarefa extends AppCompatActivity implements AdapterView.OnItemSelectedListener{


    private List<HashMap<String, String>> dadosMoradores = new ArrayList<>();


    private DbHelper crud;

    private String diasSelecionados;

    private int status_code;
    private shHelper sh;


    //Objeto de requisição padrão
    private JsonObjectRequest requisicao = null;
    private EditText edtHoraAlarme;
    private Button btnFinalizar;
    private Button btnCancelar;

    private CheckBox seg, ter, qua, qui, sex, sab, dom;

    private TextView tarefaSelecionada;

    private String dias = "";
    private String[] diasSeparados;
    private String[] meusDiasSeparados;
    private String[] arrayDiasFinal;


    private List<HashMap<String, String>> tarefaCadastrada = null;
    private boolean jaCadastrado = false;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_atb_tarefa);




        btnFinalizar = (Button) findViewById(R.id.btnCadastrarAtbTarefa);
        btnCancelar = (Button) findViewById(R.id.btnCancelarAtbTarefa);
        edtHoraAlarme = (EditText) findViewById(R.id.edtHoraAlarme);

        seg = (CheckBox) findViewById(R.id.cbSeg);
        ter = (CheckBox) findViewById(R.id.cbTer);
        qua = (CheckBox) findViewById(R.id.cbQua);
        qui = (CheckBox) findViewById(R.id.cbQui);
        sex = (CheckBox) findViewById(R.id.cbSex);
        sab = (CheckBox) findViewById(R.id.cbSab);
        dom = (CheckBox) findViewById(R.id.cbDom);

        //Atribui o nome da tarefa
        sh = new shHelper(getApplicationContext());
        crud = new DbHelper(getApplicationContext());

        dias = sh.resgatarDiasSemana();
        diasSeparados = dias.split(";");
        arrayDiasFinal = dias.split(";");

        tarefaSelecionada = (TextView) findViewById(R.id.txtTarefaSelecionada);

        tarefaSelecionada.setText("Tarefa: " + sh.resgatarTarefa(2));



        try{
            tarefaCadastrada = crud.consultarMinhasTarefas(sh.resgatarTarefa(1), sh.resgatarCampo("id_usuario"));


            if (tarefaCadastrada.size() == 0){
                Log.i("INFO: ", "TAREFA AINDA NÃO ALOCADA PARA VOCÊ");
                jaCadastrado = false;
                meusDiasSeparados = "0;0;0;0;0;0;0;".split(";");
            }else{

                        /*
                        * Alteração: Lógica de atribuição
                        * Caso 1: Tarefa não atribuida, url de atribuição
                        * Caso 2: Tarefa não atribuida, url de atualização da atribuição
                        * */
                Log.i("INFO: ", "TAREFA ALOCADA PARA VOCE NOS DIAS");
                meusDiasSeparados = tarefaCadastrada.get(0).get("dias_semana").split(";");

                Log.i("Meus dias alocados: ", tarefaCadastrada.get(0).get("dias_semana"));
                jaCadastrado = true;

            }
            Log.i("Retorno da consulta: ", tarefaCadastrada.get(0).get("dias_semana"));
        }catch (Exception e){
            Log.i("Erro Banco Atb Tarefa: ", e.toString());
        }

        verificaDias();

        btnFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //verificaDadosMorador();

                if (seg.isChecked())
                    diasSelecionados ="1;";
                else
                    diasSelecionados ="0;";
                if (ter.isChecked())
                    diasSelecionados +="1;";
                else
                    diasSelecionados +="0;";
                if (qua.isChecked())
                    diasSelecionados +="1;";
                else
                    diasSelecionados +="0;";
                if (qui.isChecked())
                    diasSelecionados +="1;";
                else
                    diasSelecionados +="0;";
                if (sex.isChecked())
                    diasSelecionados +="1;";
                else
                    diasSelecionados +="0;";
                if (sab.isChecked())
                    diasSelecionados +="1;";
                else
                    diasSelecionados +="0;";
                if (dom.isChecked())
                    diasSelecionados +="1;";
                else
                    diasSelecionados +="0;";

                Toast.makeText(getApplicationContext(), diasSelecionados, Toast.LENGTH_SHORT).show();

                atribuir();

                diasSelecionados = "";

            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        edtHoraAlarme.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (edtHoraAlarme.getRight() - edtHoraAlarme.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        DialogFragment newFragment = new TimerPickerFragment();
                        newFragment.show(getSupportFragmentManager(),"TimePicker");
                        return true;
                    }
                }
                return false;
            }
        });

    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)




        /*if (spnRepeticao.getSelectedItem().toString().equals("Todo dia")){

            seg.setClickable(false);
            ter.setClickable(false);
            qua.setClickable(false);
            qui.setClickable(false);
            sex.setClickable(false);
            sab.setClickable(false);
            dom.setClickable(false);


            seg.setChecked(true);
            ter.setChecked(true);
            qua.setChecked(true);
            qui.setChecked(true);
            sex.setChecked(true);
            sab.setChecked(true);
            dom.setChecked(true);
        }

        if (spnRepeticao.getSelectedItem().toString().equals("Dias da semana")){
            seg.setClickable(false);
            ter.setClickable(false);
            qua.setClickable(false);
            qui.setClickable(false);
            sex.setClickable(false);
            sab.setClickable(false);
            dom.setClickable(false);

            seg.setChecked(true);
            ter.setChecked(true);
            qua.setChecked(true);
            qui.setChecked(true);
            sex.setChecked(true);
            sab.setChecked(false);
            dom.setChecked(false);
        }

        if (spnRepeticao.getSelectedItem().toString().equals("Finais de semana")){
            seg.setClickable(false);
            ter.setClickable(false);
            qua.setClickable(false);
            qui.setClickable(false);
            sex.setClickable(false);
            sab.setClickable(false);
            dom.setClickable(false);

            seg.setChecked(false);
            ter.setChecked(false);
            qua.setChecked(false);
            qui.setChecked(false);
            sex.setChecked(false);
            sab.setChecked(true);
            dom.setChecked(true);
        }

        if (spnRepeticao.getSelectedItem().toString().equals("Semanal")){
            seg.setClickable(true);
            ter.setClickable(true);
            qua.setClickable(true);
            qui.setClickable(true);
            sex.setClickable(true);
            sab.setClickable(true);
            dom.setClickable(true);
        }*/
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }



    private void verificaDias() {

        if (jaCadastrado){
            int i = 0;

            for (i = 0; i < diasSeparados.length; i++){
                if (diasSeparados[i].equals("1") && meusDiasSeparados[i].equals("1")){
                    arrayDiasFinal[i] = "alocado";
                }else{
                    arrayDiasFinal[i] = diasSeparados[i];
                }
            }

            setarCheckboAtualizar();

        }else
            setarCheckBoxCadastrar();

    }

    public void onCheckboxClicked(View view){


        boolean checked = ((CheckBox)view).isChecked();

        switch (view.getId()){
            case R.id.cbSeg:
                if (checked){
                    if (diasSeparados[0].equals("1") && !meusDiasSeparados[0].equals("1")) {
                        Toast.makeText(getApplicationContext(), "Dia indisponível", Toast.LENGTH_SHORT).show();
                        seg.setChecked(false);
                    }
                }
                break;
            case R.id.cbTer:
                if (checked){
                    if (diasSeparados[1].equals("1") && !meusDiasSeparados[1].equals("1")) {
                        Toast.makeText(getApplicationContext(), "Dia indisponível", Toast.LENGTH_SHORT).show();
                        ter.setChecked(false);
                    }
                }
                break;
            case R.id.cbQua:
                if (checked){
                    if (diasSeparados[2].equals("1") && !meusDiasSeparados[2].equals("1")) {
                        Toast.makeText(getApplicationContext(), "Dia indisponível", Toast.LENGTH_SHORT).show();
                        qua.setChecked(false);
                    }
                }
                break;
            case R.id.cbQui:
                if (checked){
                    if (diasSeparados[3].equals("1") && !meusDiasSeparados[3].equals("1")) {
                        Toast.makeText(getApplicationContext(), "Dia indisponível", Toast.LENGTH_SHORT).show();
                        qui.setChecked(false);
                    }
                }
                break;
            case R.id.cbSex:
                if (checked){
                    if (diasSeparados[4].equals("1") && !meusDiasSeparados[4].equals("1")) {
                        Toast.makeText(getApplicationContext(), "Dia indisponível", Toast.LENGTH_SHORT).show();
                        sex.setChecked(false);
                    }
                }
                break;
            case R.id.cbSab:
                if (checked){
                    if (diasSeparados[5].equals("1") && !meusDiasSeparados[5].equals("1")) {
                        Toast.makeText(getApplicationContext(), "Dia indisponível", Toast.LENGTH_SHORT).show();
                        sab.setChecked(false);
                    }
                }
                break;
            case R.id.cbDom:
                if (checked){
                    if (diasSeparados[6].equals("1") && !meusDiasSeparados[6].equals("1")) {
                        Toast.makeText(getApplicationContext(), "Dia indisponível", Toast.LENGTH_SHORT).show();
                        dom.setChecked(false);
                    }
                }
                break;
        }
    }


    private void setarCheckboAtualizar(){
        if (arrayDiasFinal[0].equals("1")){
            seg.setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light_disabled));
            seg.setChecked(false);
        }else if(arrayDiasFinal[0].equals("alocado")){
            seg.setChecked(true);
        }

        if (arrayDiasFinal[1].equals("1")){
            ter.setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light_disabled));
            ter.setChecked(false);
        }else if(arrayDiasFinal[1].equals("alocado")){
            ter.setChecked(true);
        }

        if (arrayDiasFinal[2].equals("1")){
            qua.setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light_disabled));
            //seg.setClickable(false);
            qua.setChecked(false);
        }else if(arrayDiasFinal[2].equals("alocado")){
            qua.setChecked(true);
        }

        if (arrayDiasFinal[3].equals("1")){
            qui.setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light_disabled));
            //seg.setClickable(false);
            qui.setChecked(false);
        }else if(arrayDiasFinal[3].equals("alocado")){
            qui.setChecked(true);
        }

        if (arrayDiasFinal[4].equals("1")){
            sex.setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light_disabled));
            //seg.setClickable(false);
            sex.setChecked(false);
        }else if(arrayDiasFinal[4].equals("alocado")){
            sex.setChecked(true);
        }

        if (arrayDiasFinal[5].equals("1")){
            sab.setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light_disabled));
            //seg.setClickable(false);
            sab.setChecked(false);
        }else if(arrayDiasFinal[5].equals("alocado")){
            sab.setChecked(true);
        }

        if (arrayDiasFinal[6].equals("1")){
            dom.setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light_disabled));
            //seg.setClickable(false);
            dom.setChecked(false);
        }else if(arrayDiasFinal[6].equals("alocado")){
            dom.setChecked(true);
        }
    }

    private void setarCheckBoxCadastrar(){

        if (diasSeparados[0].equals("1")){
            seg.setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light_disabled));
            //seg.setClickable(false);
            seg.setChecked(false);
        }
        if (diasSeparados[1].equals("1")){
            ter.setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light_disabled));
            //seg.setClickable(false);
            ter.setChecked(false);
        }
        if (diasSeparados[2].equals("1")){
            qua.setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light_disabled));
            //seg.setClickable(false);
            qua.setChecked(false);
        }
        if (diasSeparados[3].equals("1")){
            qui.setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light_disabled));
            //seg.setClickable(false);
            qui.setChecked(false);
        }
        if (diasSeparados[4].equals("1")){
            sex.setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light_disabled));
            //seg.setClickable(false);
            sex.setChecked(false);
        }
        if (diasSeparados[5].equals("1")){
            sab.setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light_disabled));
            //seg.setClickable(false);
            sab.setChecked(false);
        }
        if (diasSeparados[6].equals("1")){
            dom.setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light_disabled));
            //seg.setClickable(false);
            dom.setChecked(false);
        }
    }





    private void atribuir(){


        server_URL url = new server_URL(getApplicationContext());
        String strURL = "";

        final Map<String, String> parametros = new HashMap<>();
        //parametros.put("id_usuario", id);
        parametros.put("id_tarefa", sh.resgatarTarefa(1));
        //parametros.put("repeticao", spnRepeticao.getSelectedItem().toString());
        parametros.put("dias_semana", diasSelecionados);



        /*
        * Verifica se o valor do id da atribuição da tarefa
        * para decidir qual url usar
        * */

        if (jaCadastrado){
            strURL = url.getURL("atualiza_atbTarefa");
        }else {
            strURL = url.getURL("atbTarefa");
        }


        sh = new shHelper(getApplicationContext());

        requisicao = new JsonObjectRequest(Request.Method.POST, strURL, new JSONObject(parametros), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                String mensagem = " ";

                Log.i("RETORNO: ", response.toString());
                if (response.has("mensagem"))
                    try {
                        mensagem = response.getString("mensagem");
                        Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_SHORT).show();
                        if (status_code == 200){
                            try {
                                String id_atb_tarefa_servidor = response.getString("id_atb_tarefa");
                                Log.i("Script => ", "Id Atb " + id_atb_tarefa_servidor);
                                cadastrarTarefaLocal(parametros, id_atb_tarefa_servidor);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Erro: ", error.toString());
                String mensagem = "";

                switch (status_code){
                    case 401:
                        mensagem = "E-mail ou senha incorretos, tente novamente !";
                        break;
                    case 500:
                        mensagem = "Falha na conexão com o servidor: " + error.toString();
                        break;
                    case 405:
                        mensagem = "Verifique o método de acesso ao servidor (POST, GET)";
                        break;

                }

                Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                sh = new shHelper(getApplicationContext());

                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + sh.resgatarToken());
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(requisicao);
    }


    public void cadastrarTarefaLocal(Map<String, String> parametros, String id_atb_tarefa_servidor){

        crud = new DbHelper(getApplicationContext());
        sh = new shHelper(getApplicationContext());

        // String passada como parâmetro para a função de criação da tabela
        String campos = "id_atb_tarefa_local INTEGER PRIMARY KEY AUTOINCREMENT, id_atb_tarefa_servidor"+
                " INTEGER, id_usuario INTEGER, id_tarefa INTEGER, titulo_tarefa VARCHAR(200), desc_tarefa VARCHAR(300), tipo_tarefa VARCHAR(40), dias_semana VARCHAR(10)";

        //String Sql para inserção dos dados na tabela Tarefas
        String strSQL = "INSERT INTO ATB_TAREFAS (id_atb_tarefa_servidor, id_usuario, id_tarefa, titulo_tarefa, desc_tarefa, tipo_tarefa, dias_semana)"+
                " VALUES ("+id_atb_tarefa_servidor+""+
                ",'"+ sh.resgatarCampo("id_usuario")+
                "','"+ sh.resgatarTarefa(1)+
                "','"+ sh.resgatarTarefa(2)+
                "', '"+sh.resgatarTarefa(3)+"'"+
                ", '"+sh.resgatarTarefa(4)+"'"+
                ", '"+parametros.get("dias_semana")+"');";

        Log.i("Quero atribuir: ", sh.resgatarTarefa(1));

        try{
            crud.criaTabela("ATB_TAREFAS", campos);
            crud.insereDados(strSQL);
            //Toast.makeText(getApplicationContext(), "Tarefa cadastrada com sucesso", Toast.LENGTH_SHORT).show();
            Log.i("Script => ", "TAREFA CADASTRADA EM ATB_TAREFAS - OK");
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "Falha ao inserir: " + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }



    public void atualizarTarefaLocal(){}



    private void carregarDados(){

        /*
        server_URL url = new server_URL(getApplicationContext());


        sh = new shHelper(getApplicationContext());

        requisicao = new JsonObjectRequest(Request.Method.GET, url.getURL("morador"), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    preencherSpinner(response.getJSONArray("moradores"));
                }catch (Exception e){
                    Log.i("ERRO NA FUNÇÃO: ", e.toString());
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }


                Log.i("RETORNO: ", response.toString());
                Log.i("CÓDIGO: ", String.valueOf(status_code));


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Erro: ", error.toString() + "Status Code: " + status_code);
                String mensagem = "";

            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + sh.resgatarToken());
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(requisicao);

        */

    }

}
