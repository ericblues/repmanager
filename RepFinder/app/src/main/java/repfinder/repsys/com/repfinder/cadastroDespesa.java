package repfinder.repsys.com.repfinder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import Servidor.server_URL;
import Shared_Preferences.shHelper;

public class cadastroDespesa extends AppCompatActivity {

    //CODIGO DE RESPOSTA DO SERVIDOR
    private int status_code;

    //Objeto de requisição padrão
    private JsonObjectRequest requisicao = null;

    private shHelper sh;


    private EditText edtTituloDespesa;
    private EditText edtDescricaoDespesa;
    private EditText edtValorDespesa;
    private Button btnCadastrar;
    private Button btnCancelar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_cadastro_despesa);


        edtTituloDespesa = (EditText) findViewById(R.id.edtTituloDespesa);
        edtDescricaoDespesa = (EditText) findViewById(R.id.edtDescricaoDespesa);
        edtValorDespesa = (EditText) findViewById(R.id.edtValorDespesa);

        btnCadastrar = (Button) findViewById(R.id.btnCadastrarDespesa);
        btnCancelar = (Button) findViewById(R.id.btnCancelarDespesa);

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastrarDespesa();
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public void cadastrarDespesa(){
        server_URL url = new server_URL(getApplicationContext());


        Map<String, String> parametros = new HashMap<>();
        parametros.put("titulo_despesa", edtTituloDespesa.getText().toString());
        parametros.put("desc_despesa", edtDescricaoDespesa.getText().toString());
        parametros.put("valor_despesa", edtValorDespesa.getText().toString());


        requisicao = new JsonObjectRequest(Request.Method.POST, url.getURL("despesa"), new JSONObject(parametros), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                String mensagem = " ";

                if (response.has("mensagem"))
                    try {
                        mensagem = response.getString("mensagem");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_LONG).show();
                Log.i("RETORNO: ", response.toString());
                Log.i("CÓDIGO: ", String.valueOf(status_code));

                if (status_code == 200) {
                    finish();
                    Intent intent = new Intent(cadastroDespesa.this, TabDespesas.class);
                    startActivity(intent);
                }
                if (status_code == 400)
                    Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Erro: ", error.toString());
                String mensagem = "";

                switch (status_code){
                    case 401:
                        mensagem = "E-mail ou senha incorretos, tente novamente !";
                        break;
                    case 500:
                        mensagem = "Falha na conexão com o servidor: " + error.toString();
                        break;
                    case 405:
                        mensagem = "Verifique o método de acesso ao servidor (POST, GET)";
                        break;

                }

                Toast.makeText(getApplicationContext(), mensagem + status_code, Toast.LENGTH_SHORT).show();

                //OUTRA MANEIRA DE PEGAR O STATUS CODE, PORÉM, APENAS DE ERRO
                //int codigo = error.networkResponse.statusCode;
            }
        }){
            //Método para pegar o status code
            //200 = sucesso
            //401 - credencial invalida
            //500 - falha de conexão com o servidor
            //405 - metodo errado, post ao inves de get ou vice versa
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }

            //Método que envia o header para o servidor
            //será necessário para enviar o token que permite as operações
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                sh = new shHelper(getApplicationContext());

                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + sh.resgatarToken());
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(requisicao);
    }
}
