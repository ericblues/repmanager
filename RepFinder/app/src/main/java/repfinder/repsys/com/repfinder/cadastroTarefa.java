package repfinder.repsys.com.repfinder;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import Db_Helper.DbHelper;
import Notificacao.EnviarNotificacao;
import Servidor.server_URL;
import Shared_Preferences.shHelper;

public class cadastroTarefa extends AppCompatActivity {


    /*
    * Foram feitas alterações:
    * O codigo de cadastro no servidor esta comentado
    * Cadastro local no sqlite realizado
    *
    * Faltam:
    *
    * Receber código da tarefa cadastrada no servidor
    * Inserir tarefa com codigo do servidor
    * Inserir codigo vazio e colocar na lista de tarefas que ainda não subiram
    *
    */

    private DbHelper crud;

    private String token = "AAAAX6RwHbQ:APA91bGkPmo8ThmNG7bHW6k5A1iZ3J0nvHyOzqtJFiyYsp3Nl2Vh7J4KGjAKZqgwIJ9VOmE1ZB5myfkAm_z8I4_YnvgLXqqojuNr77PTsc1ZEcyXnGULf249YcNwlIcA51dTy8-0BP7U";
    private String tokenGabs = "cyHEvLIMLIg:APA91bHshDQ9wssVAehnIXxzVNEyQ9fvVCFvebrOCC81knkDWhTAXKTPQVzdktITLb0shJGlNt7HFQXTSxX_EiU4QMeZySc5dfSkZCutdG2zykNQ8-bYnwmbGyTuuvHDCxJNDnzBH7vl";



    private String tokenPara = "dTBoWptL-H0:APA91bFaKfZdVcEwRjBGkCyzhKXM2w0QTz63sv-iI2LEr9rn5h5dLlFL8d78zz_xNx3NajAb9a8kRyfIkbbqPMcEXM6Du-Yw0Sk9WLQXZWkEu0MjJAUVeAp4pXYbTJ3U7GTQ8rMI3rp_";
    //CODIGO DE RESPOSTA DO SERVIDOR
    private int status_code;
    private shHelper sh;

    //Objeto de requisição padrão
    private JsonObjectRequest requisicao = null;


    private EditText edtTituloTarefa;
    private EditText edtDescricaoTarefa;
    private Spinner spnTiposTarefa;
    private Button btnCadastrar;
    private Button btnCancelar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_cadastro_tarefa);


        crud = new DbHelper(getApplicationContext());

        edtTituloTarefa = (EditText) findViewById(R.id.edtTituloTarefa);
        edtDescricaoTarefa = (EditText) findViewById(R.id.edtDescricaoTarefa);
        btnCadastrar = (Button) findViewById(R.id.btnCadastrarTarefa);
        btnCancelar = (Button) findViewById(R.id.btnCancelarTarefa);


        spnTiposTarefa = (Spinner) findViewById(R.id.spnTipoTarefas);

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.valoresTipoTareafa));
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnTiposTarefa.setAdapter(adaptador);

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cadastrarTarefa();
                //try {
                //    Request();
                //} catch (JSONException e) {
                //    e.printStackTrace();
                //}
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public void cadastrarTarefa(){

        server_URL url = new server_URL(getApplicationContext());


        sh = new shHelper(getApplicationContext());

        final Map<String, String> parametros = new HashMap<>();
        parametros.put("titulo_tarefa", edtTituloTarefa.getText().toString());
        parametros.put("desc_tarefa", edtDescricaoTarefa.getText().toString());
        parametros.put("tipo_tarefa", spnTiposTarefa.getSelectedItem().toString());


        //cadastrarTarefaLocal(parametros, 1);


        requisicao = new JsonObjectRequest(Request.Method.POST, url.getURL("tarefa"), new JSONObject(parametros), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                String mensagem = " ";

                if (response.has("mensagem"))
                    try {
                        mensagem = response.getString("mensagem");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_LONG).show();
                Log.i("RETORNO: ", response.toString());
                Log.i("CÓDIGO: ", String.valueOf(status_code));

                if (status_code == 200){

                    //EnviarNotificacao notificar = new EnviarNotificacao(getApplicationContext());


                    //cadastrarTarefaLocal(parametros, 1);

                    finish();
                    Intent intent = new Intent(cadastroTarefa.this, TabTarefas.class);
                    startActivity(intent);
                }
                if (status_code == 400)
                    Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Erro: ", error.toString());
                String mensagem = "";

                switch (status_code){
                    case 401:
                        mensagem = "E-mail ou senha incorretos, tente novamente !";
                        break;
                    case 500:
                        mensagem = "Falha na conexão com o servidor: " + error.toString();
                        break;
                    case 405:
                        mensagem = "Verifique o método de acesso ao servidor (POST, GET)";
                        break;

                }

                Toast.makeText(getApplicationContext(), mensagem + status_code, Toast.LENGTH_SHORT).show();



                //OUTRA MANEIRA DE PEGAR O STATUS CODE, PORÉM, APENAS DE ERRO
                //int codigo = error.networkResponse.statusCode;
            }
        }){
            //Método para pegar o status code
            //200 = sucesso
            //401 - credencial invalida
            //500 - falha de conexão com o servidor
            //405 - metodo errado, post ao inves de get ou vice versa
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }

            //Método que envia o header para o servidor
            //será necessário para enviar o token que permite as operações
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + sh.resgatarToken());
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(requisicao);
    }

    /*
    public void cadastrarTarefaLocal(Map<String, String> parametros, Integer id_servidor){

        // String passada como parâmetro para a função de criação da tabela
        String campos = "id_tarefa INTEGER PRIMARY KEY AUTOINCREMENT, id_tarefa_servidor"+
                " INTEGER, titulo_tarefa VARCHAR(200), desc_tarefa VARCHAR(300), tipo_tarefa VARCHAR(40)";

        //String Sql para inserção dos dados na tabela Tarefas
        String strSQL = "INSERT INTO TAREFAS (id_tarefa_servidor, titulo_tarefa, desc_tarefa, tipo_tarefa)"+
                " VALUES ("+id_servidor+", '"+
                parametros.get("titulo_tarefa")+
                "', '"+parametros.get("desc_tarefa")+"'"+
                ", '"+parametros.get("tipo_tarefa")+"');";

        try{
            crud.criaTabela("TAREFAS", campos);
            crud.insereDados(strSQL);
            Toast.makeText(getApplicationContext(), "Tarefa cadastrada com sucesso", Toast.LENGTH_SHORT).show();
            Request();
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "Falha ao inserir: " + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }*/

    private void Request() throws JSONException{

        JSONObject valores = new JSONObject();

        valores.put("title", "TITULO");
        valores.put("message", "1");
        JSONObject json = new JSONObject();

        json.put("data", valores);
        json.put("to", tokenPara);

        Log.i("Json: ", json.toString());





        requisicao = new JsonObjectRequest(Request.Method.POST, "https://fcm.googleapis.com/fcm/send", json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {


                Log.i("RETORNO: ", response.toString());
                Log.i("CÓDIGO: ", String.valueOf(status_code));

                try {
                    if (response.getString("success").equals("1"))
                        Log.i("RESPOSTA: ", "ENVIADO COM SUCESSO");
                } catch (JSONException e) {
                    Log.i("ERRO: ", e.toString());
                    e.printStackTrace();
                }

                Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Erro: ", error.toString());
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response){
                if (response != null){
                    status_code = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "key=" + token);
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(requisicao);
    }
}
